/* LIBPDB -- The Procedural Database Library
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>

#include "config.h" 

#include "pdbtypes.h"

#include "pdbargs.h"
#include "pdbproc.h"

#include "pdbdestroy.h"

static GObjectClass *parent_class = NULL; 

static void pdb_args_init       (PdbArgs *args);
static void pdb_args_class_init (PdbArgsClass *klass);
static void pdb_args_finalize   (GObject*);

GType
pdb_args_get_type (void)
{
  static GType pdb_type = 0;
        
  if (! pdb_type)
    {
      static const GTypeInfo pdb_args_info =
        {
          sizeof (PdbArgsClass),
          (GBaseInitFunc) NULL,
          (GBaseFinalizeFunc) NULL,
          (GClassInitFunc) pdb_args_class_init,
          NULL,           /* class_finalize */
          NULL,           /* class_data     */
          sizeof (PdbArgs),
          0,              /* n_preallocs    */
          (GInstanceInitFunc) pdb_args_init,
        };

      pdb_type = g_type_register_static (G_TYPE_OBJECT,
                                         "PdbArgs", 
                                          &pdb_args_info, 0);
    }
        
  return pdb_type;
}


void
pdb_args_init (PdbArgs *args)
{
  args->values = g_hash_table_new_full(g_str_hash, g_str_equal, 
                                       NULL, pdb_destroy_value);
}

void
pdb_args_class_init (PdbArgsClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  parent_class = g_type_class_peek_parent (klass);
  object_class->finalize = pdb_args_finalize;
}

void
pdb_args_finalize (GObject* obj)
{
  PdbArgs* args = PDB_ARGS (obj);

  g_hash_table_destroy (args->values);

  G_OBJECT_CLASS (parent_class)->finalize (obj);
}


/**
 * pdb_args_new:
 *
 * Creates a new and empty #PdbArgs
 *
 * Returns: a newly allocated #PdbArgs.
 */

PdbArgs *
pdb_args_new (void) {
  /* FIXME: allow arg construction as varargs here */
  return g_object_new(PDB_TYPE_ARGS, NULL);
}

/*
 * pdb_args_add
 * @args: The #PdbArgs
 * @proc: The #PdbProc 
 * @...: an argument list of zero or more NAME, VALUE pairs
 *
 * Adds a list of arguments to @args. The argument list ends when NAME is NULL.
 */
void
pdb_args_add(PdbArgs* args, PdbProc* proc, ...)
{
        va_list va;

        va_start(va, proc);
        pdb_args_add_valist(args, proc, va);
        va_end(va);
}

/*
 * pdb_args_add_valist
 * @args: The #PdbArgs
 * @proc: The #PdbProc
 * @va_list: an argument list of zero or more NAME, VALUE pairs
 *
 * Adds a list of arguments to @args. The argument list ends when NAME is NULL.
 */
void
pdb_args_add_valist(PdbArgs* args, PdbProc* proc, va_list list)
{
  PdbArgType type;
  gchar* name;
  GValue* value;

  gint v_int;
  gdouble v_double;
  gpointer v_pointer;

  for(;;)
    {
      name = va_arg (list, gchar*);
      if (!name)
        break;

      if (!pdb_proc_lookup_param (proc, name, NULL, &type))
        {
          g_warning("Argument '%s' not found in procedure '%s'", 
                    name, pdb_proc_get_name(proc));

          break;
        }

      /* Read the value acording to the type of the parameter */
      value = g_new0 (GValue, 1);
      
      switch(type)
        {
          case PDB_INT:
          v_int = va_arg(list, gint);
          g_value_init(value, G_TYPE_INT);
          g_value_set_int(value, v_int);
          break;

          case PDB_DOUBLE:
          v_double = va_arg(list, gdouble);
          g_value_init(value, G_TYPE_DOUBLE);
          g_value_set_double(value, v_double);
          break;

          case PDB_POINTER:
          v_pointer = va_arg(list, gpointer);
          g_value_init(value, G_TYPE_POINTER);
          g_value_set_pointer(value, v_pointer);
          break;

          case PDB_STRING:
          v_pointer = va_arg(list, gpointer);
          g_value_init(value, G_TYPE_STRING);
          g_value_set_string(value, v_pointer);
          break;

          default:
          g_warning("Unkown type (%d) for parameter %s", (gint)type, name);
          g_free(value);
          value = NULL;
          break;
        }

      if(value)
        g_hash_table_insert(args->values, name, value);
      else
        break;
    }
}

/*
 * pdb_args_lookup
 * @args: The #PdbArgs
 * @name: Name of the argument to look up
 * @value: Variable where the #GValue will be copied. Note: don't modify or free its content
 *
 * This function looks up in the arguments and, if @name if found, fills @value
 * with its value. @value has to be unset when it is no longer needed
 *
 * Returns: TRUE if @name was found, FALSE if not
 */
gboolean
pdb_args_lookup (PdbArgs      *args,
                 gchar const  *name, 
                 GValue      **value)
{
  gpointer v;

  g_return_val_if_fail (args != NULL, FALSE);
  g_return_val_if_fail (PDB_IS_ARGS (args), FALSE);

  v = g_hash_table_lookup (args->values, name);
  
  if (!v)
    return FALSE;

  if (value)
    *value = (GValue *) v;

  return TRUE;
}
