/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <stdarg.h>
#include <string.h>
#include <sys/types.h>

#include <glib.h>

#include "config.h"

#include "pdbtypes.h"

#include "pdbproc.h"

#include "pdbdestroy.h"

#include "pdbintl.h"

#define QUARK_FROM_STRING_ARGTYPE g_quark_from_string ("PdbArgType")

static void pdb_proc_init       (PdbProc* proc);
static void pdb_proc_class_init (PdbProcClass* klass);
static void pdb_proc_finalize   (GObject* object);

static PdbResult* pdb_proc_default_exec (PdbProc *proc, 
                                         PdbArgs *args);


GObjectClass* parent_class = NULL;

static void 
pdb_proc_finalize (GObject* object)
{
  PdbProc *proc;

  proc = PDB_PROC (object);

  if (proc->values)
    {
      g_hash_table_destroy (proc->values);
      proc->values = NULL; /* protect agaist double-frees */
    }

  if (proc->args)
    {
      g_hash_table_destroy (proc->args);
      proc->args = NULL; 
    }
    

  if(proc->name) 
    {
      g_free(proc->name);
      proc->name = NULL;
    }

  if(proc->namespace)
    {
      g_free(proc->namespace);
      proc->namespace = NULL;
    }
}

static PdbResult*
pdb_proc_default_exec (PdbProc *proc, 
                       PdbArgs  *args)
{
  /* Simply call the funtion */
  return (* proc->exec) (proc, args);
}

static void
pdb_proc_class_init (PdbProcClass* klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  parent_class = g_type_class_peek_parent (klass);
  object_class->finalize = pdb_proc_finalize;

  klass->exec = pdb_proc_default_exec;
}

static void
pdb_proc_init (PdbProc* proc)
{
  proc->namespace = NULL;
  proc->name      = NULL;
  proc->values    = g_hash_table_new(g_str_hash, g_str_equal);
  proc->args      = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, pdb_destroy_param_spec);
  proc->exec      = NULL;
}

GType
pdb_proc_get_type (void)
{
  static GType pdb_proc_type = 0;

  if (! pdb_proc_type)
    {
      static const GTypeInfo pdb_proc_info =
        {
           sizeof (PdbProcClass),
           (GBaseInitFunc) NULL,
           (GBaseFinalizeFunc) NULL,
           (GClassInitFunc) pdb_proc_class_init,
           NULL,           /* class_finalize */
           NULL,           /* class_data     */
           sizeof (PdbProc),
           0,              /* n_preallocs    */
           (GInstanceInitFunc) pdb_proc_init,
        };

      pdb_proc_type = g_type_register_static (G_TYPE_OBJECT,
                                              "PdbProc", 
                                               &pdb_proc_info, 0);
    }

  return pdb_proc_type;
}


/**
 * pdb_proc_new
 * @namespace: namespace for this procedure. It can be NULL
 * @name: Name of the procedure in the PDB. It can't be NULL nor empty
 * @fn: A #PdbCallback, the function to call when the procedure is executed
 *
 * Creates a new procedure.
 *
 * Returns: A new #PdbProc
 */
PdbProc*
pdb_proc_new(gchar       *namespace,
             gchar       *name,
             PdbCallback  exec)
{
  PdbProc* proc;

  g_return_val_if_fail(name != NULL, NULL);
  g_return_val_if_fail(strlen(name) > 0, NULL);

  proc = PDB_PROC(g_object_new(PDB_TYPE_PROC, NULL));
  proc->name = g_strdup(name);
  proc->namespace = g_strdup(namespace);
  proc->exec = exec;

  return proc;
}


/**
 * pdb_proc_exec:
 * @proc: Description of the procedure.
 * @args: Arguments to the function
 *
 * This function invokes the procedure represented by the #PdbProc. It is 
 * purely virtual. 
 *
 * Returns: A #PdbResult with the values. You have to free it with pdb_result_free function
 */

PdbResult*
pdb_proc_exec (PdbProc *proc, 
               PdbArgs *args)
{
  /* GParamSpec* pspec; */

  g_return_val_if_fail (PDB_IS_PROC(proc), NULL);

  /* FIXME: Are all required arguments given */

  /* Derived objects from PdbProc have to be able to 
   * use its own exec() method */
  return PDB_PROC_GET_CLASS (proc)->exec (proc, args);
}

/**
 * pdb_proc_get_namespace:
 * @proc: The procedure.
 *
 * Returns: The name of the procedure.
 */
G_CONST_RETURN gchar* 
pdb_proc_get_namespace (PdbProc* proc)
{
  g_return_val_if_fail (proc != NULL, NULL);
  g_return_val_if_fail (PDB_IS_PROC(proc), NULL);

  return proc->namespace;
}

/**
 * pdb_proc_get_name:
 * @proc: The procedure.
 *
 * Returns: The name of the procedure.
 */
G_CONST_RETURN gchar* 
pdb_proc_get_name (PdbProc* proc)
{
  g_return_val_if_fail (proc != NULL, NULL);
  g_return_val_if_fail (PDB_IS_PROC(proc), NULL);
  
  return proc->name;
}

/**
 * pdb_proc_add_param_int:
 * @proc: The description of the procedure.
 * @name: Name of the parameter
 * @requiered: TRUE is this parameter is required to exec the procedure
 * @blurb: a short description of the procedure. It can be NULL
 * @miminum: Minimun value for this param
 * @maximun: Maximun value for this param
 * @default_value: Value to use when no one is given.
 *
 * This function adds a new parameter to the procedure. The new parameter will be
 * an integer (gint)
 */

void
pdb_proc_add_param_int (PdbProc  *proc, 
                        gchar    *name, 
                        gboolean  required, 
                        gchar    *blurb,
                        gint      mininum,
                        gint      maximun,
                        gint      default_value)
{
        GParamSpec* new_param;
        GParamFlags flags;

        g_return_if_fail (PDB_IS_PROC (proc));

        /* flag to this param */
        flags = G_PARAM_READABLE|G_PARAM_WRITABLE;
        if(required)
                flags |= PDB_ARG_REQUIRED;

        /* Create this new param */
        new_param = g_param_spec_int(name, 
                                     NULL, /* Do we need a nickme? */
                                     blurb,
                                     mininum,
                                     maximun,
                                     default_value,
                                     flags);

        g_hash_table_insert (proc->args, (gpointer)name, (gpointer)new_param);
        g_param_spec_set_qdata (new_param, QUARK_FROM_STRING_ARGTYPE, (gpointer)PDB_INT);
}

/**
 * pdb_proc_add_param_double:
 * @proc: The description of the procedure.
 * @name: Name of the parameter
 * @requiered: TRUE is this parameter is required to exec the procedure
 * @blurb: a short description of the procedure. It can be NULL
 * @miminum: Minimun value for this param
 * @maximun: Maximun value for this param
 * @default_value: Value to use when no one is given.
 *
 * This function adds a new parameter to the procedure. The new parameter will be
 * a real number, double presicion (gdouble)
 */

void
pdb_proc_add_param_double (PdbProc *proc, 
                          gchar    *name, 
                          gboolean  required, 
                          gchar    *blurb,
                          gdouble   mininum,
                          gdouble   maximun,
                          gdouble   default_value)
{
  GParamSpec* new_param;
  GParamFlags flags;

  g_return_if_fail (proc != NULL);
  g_return_if_fail (PDB_IS_PROC (proc));

   /* flag to this param */
   flags = G_PARAM_READABLE|G_PARAM_WRITABLE;
     if(required)
       flags |= PDB_ARG_REQUIRED;

   /* Create this new param */
   new_param = g_param_spec_double (name, 
                                    NULL, /* Do we need a nickme? */
                                    blurb,
                                    mininum,
                                    maximun,
                                    default_value,
                                    flags);

  g_hash_table_insert (proc->args, (gpointer)name, (gpointer)new_param);
  g_param_spec_set_qdata (new_param, QUARK_FROM_STRING_ARGTYPE, (gpointer)PDB_DOUBLE);
}

/**
 * pdb_proc_add_param_string:
 * @proc: The description of the procedure.
 * @name: Name of the parameter
 * @requiered: TRUE is this parameter is required to exec the procedure
 * @blurb: a short description of the procedure. It can be NULL
 * @default_value: Value to use when no one is given.
 *
 * This function adds a new parameter to the procedure. The new parameter will be
 * an string (gchar*). The default value can be NULL. If it is not NULL, it will be
 * duplicated (with g_strdup) to store in the parameter.
 */

void
pdb_proc_add_param_string (PdbProc  *proc, 
                           gchar    *name, 
                           gboolean  required, 
                           gchar    *blurb,
                           gchar    *default_value)
{
  GParamSpec* new_param;
  GParamFlags flags;

  g_return_if_fail(PDB_IS_PROC(proc));

  /* flag to this param */
  flags = G_PARAM_READABLE|G_PARAM_WRITABLE;
  if(required)
    flags |= PDB_ARG_REQUIRED;

  /* Create this new param */
  new_param = g_param_spec_string (name, 
                                   NULL, /* Do we need a nickme? */
                                   blurb,
                                   default_value,
                                   flags);

  g_hash_table_insert (proc->args, (gpointer)name, (gpointer)new_param);
  g_param_spec_set_qdata (new_param, QUARK_FROM_STRING_ARGTYPE, (gpointer)PDB_STRING);
}

/**
 * pdb_proc_add_param_pointer:
 * @proc: The description of the procedure.
 * @name: Name of the parameter
 * @requiered: TRUE is this parameter is required to exec the procedure
 * @blurb: a short description of the procedure. It can be NULL
 * @default_value: Value to use when no one is given.
 *
 * This function adds a new parameter to the procedure. 
 */

void
pdb_proc_add_param_pointer (PdbProc* proc, 
                            gchar* name, 
                            gboolean required, 
                            gchar* blurb)
{
  GParamSpec* new_param;
  GParamFlags flags;

  g_return_if_fail(PDB_IS_PROC(proc));

  /* flag to this param */
  flags = G_PARAM_READABLE|G_PARAM_WRITABLE;
  if(required)
    flags |= PDB_ARG_REQUIRED;

  /* Create this new param */
  new_param = g_param_spec_pointer (name, 
                                    NULL, /* Do we need a nickme? */
                                    blurb,
                                    flags);

  g_hash_table_insert (proc->args, (gpointer)name, (gpointer)new_param);
  g_param_spec_set_qdata (new_param, QUARK_FROM_STRING_ARGTYPE, (gpointer)PDB_POINTER);
}

/**
 * pdb_proc_add_return_type
 * @proc: The procedure
 * @name: The new for this returned value.
 * @type: A #PdbArgType
 *
 * Add a new type for the returned value list.
 */
void
pdb_proc_add_return_type (PdbProc    *proc, 
                          gchar      *name, 
                          PdbArgType  type)
{
  g_return_if_fail(PDB_IS_PROC (proc));

  g_hash_table_insert(proc->values, name, (gpointer) type);
}

/**
 * pdb_proc_lookup_param
 * @proc: The procedure
 * @name: Parameter to look up
 * @pspec: A #GParamSpec to fill if the parameter is found. Can be NULL if you
 *         only want to know if the parameter exists
 * @type: A #PdbArgType where the paremeter type will be copie. It can be NULL
 *
 * Returns: TRUE if the parameter was found. FALSE if not
 */
gboolean
pdb_proc_lookup_param (PdbProc     *proc,
                       gchar       *name, 
                       GParamSpec **pspec, 
                       PdbArgType  *type)
{
  GParamSpec* val;

  g_return_val_if_fail (PDB_IS_PROC (proc), FALSE);

  val = g_hash_table_lookup(proc->args, name);
  if(val)
    {
      if(pspec)
      *pspec = val;

      if(type)
        *type = (PdbArgType) g_param_spec_get_qdata(val, QUARK_FROM_STRING_ARGTYPE);

        return TRUE;
      }
        
  return FALSE;
}

/*
 * PdbResult: Store the values returned by a procedure
 */

/**
 * pdb_result_new:
 *
 * Returns: A new PdbResult object
 */
PdbResult*
pdb_result_new (void)
{
  PdbResult* result;

  result = g_new0 (PdbResult, 1);
  result->values = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, pdb_destroy_value);
  result->error = NULL;

  return result;
}

/**
 * pdb_result_new_error:
 * @error: The error that you want to asing to this PdbResult
 *
 * Use this function is you want to set a predefined error. If you want 
 * you custom error, create a new PdbResult with pdb_result_new and set
 * the error member with g_error_* API
 * 
 * Returns: A new PdbResult with a predefined error
 */
PdbResult *
pdb_result_new_error (PdbResultError error)
{
  PdbResult* result;
  GQuark quark;

  result = pdb_result_new();
  quark = g_quark_from_string("Pdb");
  
  switch(error)
    {
      case PDB_ERROR_NOT_EXECUTABLE:
      result->error = g_error_new_literal (quark, 
                                           PDB_ERROR_NOT_EXECUTABLE,
                                           _("At this moment, this procedure can't be executed"));
      break;

      case PDB_ERROR_INVALID_ARGS:
      result->error = g_error_new_literal(quark, 
                                          PDB_ERROR_INVALID_ARGS,
                                          _("The arguments are not valid"));
      break;

      case PDB_ERROR_UNKNOWN:
      result->error = g_error_new_literal(quark, 
                                          PDB_ERROR_UNKNOWN,
                                          _("Unknown error"));
      break;

    }

  return result;
}

/**
 * pdb_result_free
 * @result: A #PdbResult
 *
 * Free the resources asociates to PdbResult
 */
void
pdb_result_free (PdbResult* result)
{
  g_return_if_fail(result != NULL);
  if(result->values) 
    {
      g_hash_table_destroy (result->values);
      result->values = NULL;
    }
    

  if(result->error)
    {
      g_error_free (result->error);
      result->error = NULL;
    }
}

/**
 * pdb_result_add_value_int:
 * @result: The #PdbResult where store the value
 * @name: Name of the value
 * @data: Value
 *
 */
void 
pdb_result_add_value_int (PdbResult *result, 
                          gchar     *name, 
                          gint       data)
{
  GValue* value;

  value = g_new0 (GValue, 1);
  g_value_init (value, G_TYPE_INT);
  g_value_set_int (value, data);
  g_hash_table_insert (result->values, (gpointer)name, value);
}

/**
 * pdb_result_add_value_string:
 * @result: The #PdbResult where store the value
 * @name: Name of the value
 * @data: Value
 *
 */
void
pdb_result_add_value_string (PdbResult *result, 
                             gchar     *name, 
                             gchar     *data)
{
  GValue* value;

  value = g_new0 (GValue, 1);
  g_value_init (value, G_TYPE_STRING);
  g_value_set_string (value, data);
  g_hash_table_insert (result->values, (gpointer)name, value);
}

/**
 * pdb_result_add_value_double:
 * @result: The #PdbResult where store the value
 * @name: Name of the value
 * @data: Value
 *
 */
void
pdb_result_add_value_double(PdbResult* result, gchar* name, gdouble data)
{
  GValue* value;

  value = g_new0(GValue, 1);
  g_value_init(value, G_TYPE_DOUBLE);
  g_value_set_double(value, data);
  g_hash_table_insert(result->values, (gpointer)name, value);
}

/**
 * pdb_result_add_value_pointer:
 * @result: The #PdbResult where store the value
 * @name: Name of the value
 * @data: Value
 *
 */
void 
pdb_result_add_value_pointer(PdbResult* result, gchar* name, gpointer data)
{
        GValue* value;

        value = g_new0(GValue, 1);
        g_value_init(value, G_TYPE_POINTER);
        g_value_set_pointer(value, data);
        g_hash_table_insert(result->values, (gpointer)name, value);
}

/**
 * pdb_result_has_error:
 * @result: the result of a procedure
 *
 * Returns: TRUE is @result has an error. FALSE if it is a normal result
 */
gboolean
pdb_result_has_error(PdbResult* result)
{
        g_return_val_if_fail(result != NULL, FALSE);
        return result->error != NULL;
}

/**
 * pdb_result_lookup:
 * @result: the result of a procedure
 * @name: name of the value
 * @value: the place where store the value
 *
 * Returns: TRUE is the value was found
 */
gboolean
pdb_result_lookup(PdbResult* result, gchar* name, GValue** value)
{
	GValue* val;
        g_return_val_if_fail(result != NULL, FALSE);

	val = (GValue*)g_hash_table_lookup(result->values, name);

	if(!val)
		return FALSE;

	if(value)
		*value = val;

        return TRUE;
}

