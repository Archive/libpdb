/* LIBPDB - The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_PROC_H__
#define __PDB_PROC_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define PDB_TYPE_PROC            (pdb_proc_get_type ())
#define PDB_PROC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PDB_TYPE_PROC, PdbProc))
#define PDB_PROC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PDB_TYPE_PROC, PdbProcClass))
#define PDB_IS_PROC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PDB_TYPE_PROC))
#define PDB_IS_PROC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PDB_TYPE_PROC))
#define PDB_PROC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PDB_TYPE_PROC, PdbProcClass))

/* flags to be used with */
#define PDB_ARG_REQUIRED (1 << (G_PARAM_USER_SHIFT + 1))

typedef PdbResult* (*PdbCallback) (PdbProc *,PdbArgs *);

/*  Structure for a procedure  */
struct _PdbProc {
  GObject     parent_instance;

  /*  Procedure information  */
  gchar       *namespace;     /*  Namespace procedure belongs to  */
  gchar       *name;          /*  Procedure name  */

  GHashTable  *args;   /* Input arguments  */
  GHashTable  *values; /* Output values    */
  PdbCallback  exec;   /* Function to call */
};


struct _PdbProcClass {
  GObjectClass  parent_class;

  PdbCallback exec;
};

PdbProc * pdb_proc_new      (gchar* namespace, 
                             gchar* name, 
                             PdbCallback exec);
GType     pdb_proc_get_type (void) G_GNUC_CONST;

PdbResult *            pdb_proc_exec          (PdbProc     *proc, 
                                               PdbArgs     *args);
G_CONST_RETURN gchar * pdb_proc_get_namespace (PdbProc     *proc);
G_CONST_RETURN gchar * pdb_proc_get_name      (PdbProc     *proc);
gboolean               pdb_proc_lookup_param  (PdbProc     *proc, 
                                               gchar       *name, 
                                               GParamSpec **spec, 
                                               PdbArgType  *type);

void pdb_proc_add_return_type   (PdbProc    *proc, 
                                 gchar      *name,
                                 PdbArgType  type);
void pdb_proc_add_param_int     (PdbProc    *name, gchar*, gboolean, gchar*, gint, gint, gint);
void pdb_proc_add_param_double  (PdbProc*, gchar*, gboolean, gchar*, gdouble, gdouble, gdouble);
void pdb_proc_add_param_string  (PdbProc*, gchar*, gboolean, gchar*, gchar*);
void pdb_proc_add_param_pointer (PdbProc*, gchar*, gboolean, gchar*);


/* 
 * PdbResult is used to store the values returned by
 * the procedures
 */

typedef enum {
  PDB_ERROR_INVALID_ARGS = 1,
  PDB_ERROR_NOT_EXECUTABLE,
  PDB_ERROR_UNKNOWN,
} PdbResultError;

struct _PdbResult {
        GHashTable *values;
        GError *error;
};

PdbResult* pdb_result_new();
PdbResult* pdb_result_new_error(PdbResultError error);
gboolean pdb_result_has_error(PdbResult* result);
void pdb_result_free(PdbResult* result);
void pdb_result_add_value_int(PdbResult* result, gchar* name, gint data);
void pdb_result_add_value_string(PdbResult* result, gchar* name, gchar* data);
void pdb_result_add_value_double(PdbResult* result, gchar* name, gdouble data);
void pdb_result_add_value_pointer(PdbResult* result, gchar* name, gpointer data);
gboolean pdb_result_lookup(PdbResult* result, gchar* name, GValue** value);

G_END_DECLS

#endif  /*  __PDB_H__  */
