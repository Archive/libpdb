/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_ACCUMULATOR_H__
#define __PDB_ACCUMULATOR_H__


G_BEGIN_DECLS

gboolean pdb_list_accumulator         (GSignalInvocationHint *hint,
                                       GValue                *return_accumulator,
                                       const GValue          *handler_return,
                                       gpointer               user_data);
gboolean pdb_stop_on_true_accumulator (GSignalInvocationHint *hint,
                                       GValue                *return_accumulator,
                                       const GValue          *handler_return,
                                       gpointer               user_data);
gboolean pdb_object_accumulator       (GSignalInvocationHint *hint,
                                       GValue                *return_accumulator,
                                       const GValue          *handler_return,
                                       gpointer               user_data);

G_END_DECLS

#endif  /*  __PDB_ACCUMULATOR_H__  */
