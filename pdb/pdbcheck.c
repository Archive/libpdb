/* LIBPDB - The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>

#include "pdb.h"

#define PDB_GET_VAL(name, val) \
        if(!pdb_args_lookup(args, name, val)) \
        { \
                g_print("E: %s argument not found for %s\n", name, pdb_proc_get_name(proc)); \
                return pdb_result_new_error(PDB_ERROR_INVALID_ARGS); \
        }

/* test procedures */

void        register_simple (Pdb     *pdb);
PdbResult * simple_exec     (PdbProc *proc, 
                             PdbArgs *args);

void        register_sum    (Pdb     *pdb);
PdbResult * sum_exec        (PdbProc *proc, 
                             PdbArgs *args);

void        register_math   (Pdb     *pdb);
PdbResult * math_exec       (PdbProc *proc, 
                             PdbArgs *args);

/* signal handlers */

static PdbProc *  lookup_proc       (Pdb          *pdb, 
                                     const gchar  *name, 
                                     gpointer      user_data);
static gboolean   register_proc     (Pdb          *pdb, 
                                     PdbProc      *proc, 
                                     gpointer      user_data);
static void       proc_unregistered (Pdb          *pdb, 
                                     PdbProc      *proc, 
                                     gpointer      user_data);
static GObject *  unregister_proc   (Pdb          *pdb, 
                                     const gchar  *name, 
                                     gpointer      user_data);
static void       proc_registered   (Pdb          *pdb, 
                                     PdbProc      *proc, 
                                     gpointer      user_data);

/*
 * my::hello
 * Simplest procedure, with no arguments and no return values
 */
PdbResult *
simple_exec (PdbProc *proc, 
             PdbArgs *args)
{
  g_print("Hi people\n");

  return NULL;
}

void
register_simple (Pdb *pdb)
{
  PdbProc *proc;
  proc = pdb_proc_new ("pdbcheck", "hello", simple_exec);

  if (!proc)
    g_error ("could not create simple PdbProc");
  
  if (!pdb_register (pdb, proc))
    g_error ("registration failed");
  
  g_object_unref (G_OBJECT (proc));
}

/*
 * my:sum
 * A more complex procedure: adds two numbers and return the result
 */
PdbResult* 
sum_exec (PdbProc *proc, 
          PdbArgs *args)
{
  GValue *sum1, *sum2;
  PdbResult* result;
  gint a, b;

  PDB_GET_VAL("sum1", &sum1);
  PDB_GET_VAL("sum2", &sum2);

  a = g_value_get_int (sum1);
  b = g_value_get_int (sum2);

  result = pdb_result_new ();
  pdb_result_add_value_int (result, "result", a + b);
  return result;
}

void
register_sum (Pdb *pdb)
{
  PdbProc *proc;
  
  proc = pdb_proc_new ("pdbcheck", "sum", sum_exec);
  
  pdb_proc_add_param_int (proc, "sum1", TRUE, "<b>", 0, 0, 0);
  pdb_proc_add_param_int (proc, "sum2", TRUE, "<b>", 0, 0, 0);
  pdb_proc_add_return_type (proc, "result", PDB_INT);
  
  pdb_register (pdb, proc);
  
  g_object_unref (G_OBJECT (proc));
}

/*
 * my:math
 * A process with a double argument.
 *   my:math(x, y) = x*x + y
 */
PdbResult * 
math_exec (PdbProc *proc, 
           PdbArgs *args)
{
  GValue *dbl, *integer;
  PdbResult* result;
  gint a;
  gdouble b;

  PDB_GET_VAL ("real", &dbl);
  PDB_GET_VAL ("integer", &integer);

  a = g_value_get_double (dbl);
  b = g_value_get_int (integer);

  result = pdb_result_new ();
  pdb_result_add_value_double (result, "result", a*a + b);
  return result;
}

void
register_math (Pdb* pdb)
{
  PdbProc *proc;
  
  proc = pdb_proc_new ("pdbcheck", "math", math_exec);
  
  pdb_proc_add_param_double (proc, "real",    TRUE, "<b>", 0, 0, 0);
  pdb_proc_add_param_int    (proc, "integer", TRUE, "<b>", 0, 0, 0);
  pdb_proc_add_return_type  (proc, "result",  PDB_DOUBLE);

  pdb_register(pdb, proc);
  
  g_object_unref(G_OBJECT(proc));
}


 
static PdbProc *
lookup_proc (Pdb          *pdb,
             const gchar  *name,
             gpointer      user_data)
{
  gboolean *called = user_data;

  *called = TRUE;

  g_print ("lookup_proc called for %s.\n", name);

  return NULL;
}

static gboolean 
register_proc (Pdb       *pdb,
               PdbProc   *proc,
               gpointer   user_data)
{
  gboolean *called = user_data;

  *called = TRUE;
  g_print ("register_proc called for %s.\n", proc->name);

  return FALSE;
}

static void
proc_unregistered (Pdb      *pdb,
                   PdbProc  *proc,
                   gpointer  user_data)
{
  gboolean *called = user_data;
  g_print ("proc_unregistered called for %s.\n", proc->name);

  *called = TRUE;
}
                                                                                                                                                                                                            
static GObject * 
unregister_proc (Pdb          *pdb,
                 const gchar  *name,
                 gpointer      user_data)
{
  gboolean *called = user_data;

  *called = TRUE;
  g_print ("unregister_proc called for %s.\n", name);

  return NULL;
}

static void
proc_registered (Pdb      *pdb,
                 PdbProc  *proc,
                 gpointer  user_data)
{
  gboolean *called = user_data;

  *called = TRUE;
  g_print ("proc_registered called for %s.\n", proc->name);
}


/*
 * main
 */

int 
main (int argc, char **argv)
{
  Pdb *pdb;
  GValue* val;
  PdbResult* result;

  gboolean register_proc_called = FALSE;
  gboolean proc_registered_called = FALSE;
  gboolean lookup_proc_called = FALSE;
  gboolean unregister_proc_called = FALSE;
  gboolean proc_unregistered_called = FALSE;

  g_type_init();
  pdb = pdb_new(); 

  /* check signal functionality */

  g_signal_connect (G_OBJECT (pdb), "lookup-proc",
                    G_CALLBACK (lookup_proc),
                    &lookup_proc_called);
  g_signal_connect (G_OBJECT (pdb), "register-proc",
                    G_CALLBACK (register_proc),
                    &register_proc_called);
  g_signal_connect (G_OBJECT (pdb), "proc-registered",
                    G_CALLBACK (proc_registered),
                    &proc_registered_called);
  g_signal_connect (G_OBJECT (pdb), "unregister-proc",
                    G_CALLBACK (unregister_proc),
                    &unregister_proc_called);
  g_signal_connect (G_OBJECT (pdb), "proc-unregistered",
                    G_CALLBACK (proc_unregistered),
                    &proc_unregistered_called);
  

  /* Register the procedures */
  g_print ("Testing procedure registration\n");
  register_sum(pdb);
  register_simple(pdb);
  register_math(pdb);

  g_print ("\n\nTesting procedure execution.\n");

  /*
   * A normal action, simply call the procedure. 
   */
  g_print ("This procedure should print \"Hi, people\"\n");
  pdb_execute_by_name (pdb, "hello", NULL);

  /* 
   * Add a parameter called 'aa', but 'hello' has no arguments. A message has
   * to be printed with g_warning 
   */
  g_print ("\nThis procedure should print \"Hi, people\" as well,\n");
  g_print ("but print out a warning about \"aa\" not being a valid argument.\n");
  pdb_execute_by_name (pdb, "hello", NULL);
  pdb_execute_by_name(pdb, "hello", "aa", NULL);

  /* 
   * A normal action: call the 'sum' proc with their 2 parameters. The result
   * is printed by main()
   */
  result = pdb_execute_by_name (pdb, "sum", "sum1", 14, "sum2", 10, NULL);
  if(pdb_result_has_error (result))
    {
      g_print ("E: error in sum: %s\n", result->error->message);
    } 
  else
    {
      if (pdb_result_lookup (result, "result", &val))
        g_print ("Number: %d\n", g_value_get_int (val));
      else
        g_print ("Error: 'result' wasn't found");
    }

  pdb_result_free (result);

  /*
   * Call to sum without parameters, a message has to be printed and result has
   * to have the error
   */
  result = pdb_execute_by_name (pdb, "sum", NULL);
  if (pdb_result_has_error (result))
    g_print ("Error from sum: %s\n", result->error->message);
                  
    pdb_result_free(result);

  /*
   * Call to math, with a double an a int
   */
  result = pdb_execute_by_name(pdb, "math", "real", 4.5, "integer", 10, NULL);
  if(pdb_result_has_error (result))
    {
      g_print("E: error in sum: %s\n", result->error->message);
    } 
  else
    {
      if(pdb_result_lookup(result, "result", &val))
        g_print("Number: %f\n", g_value_get_double(val));
      else
        g_print("Error: 'result' wasn't found");
    }

  pdb_result_free(result);

  if (!pdb_unregister (pdb, "hello"))
    g_error ("unregister failed");

  /*
   * Free the resources
   */
  g_object_unref(pdb);
  
  if (!register_proc_called)
    g_error ("register_proc not signaled");
    
  if (!proc_registered_called)
    g_error ("proc-registered not signaled");

  if (!lookup_proc_called)
    g_error ("lookup-proc not signaled");

  if (!unregister_proc_called)
    g_error ("unregister_proc not signaled");
    
  if (!proc_unregistered_called)
    g_error ("proc-unregistered not signaled");
    
  return 0;
}
