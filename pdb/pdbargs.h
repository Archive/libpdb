/* LIBPDB - The Procedural Database Library
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_ARGS_H__
#define __PDB_ARGS_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define PDB_TYPE_ARGS            (pdb_args_get_type ())
#define PDB_ARGS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PDB_TYPE_ARGS, PdbArgs))
#define PDB_ARGS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PDB_TYPE_ARGS, PdbArgsClass))
#define PDB_IS_ARGS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PDB_TYPE_ARGS))
#define PDB_IS_ARGS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PDB_TYPE_ARGS))
#define PDB_ARGS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PDB_TYPE_ARGS, PdbArgsClass))

struct _PdbArgs 
{
  GObject     parent;

  GHashTable *values;
};

struct _PdbArgsClass
{
  GObjectClass parent;
};

PdbArgs* pdb_args_new        (void) G_GNUC_CONST;
void     pdb_args_add        (PdbArgs      *args, 
                              PdbProc      *proc, 
                              ...);
void     pdb_args_add_valist (PdbArgs      *args, 
                              PdbProc      *proc, 
                              va_list       list);
gboolean pdb_args_lookup     (PdbArgs      *args, 
                              gchar const  *name, 
                              GValue      **value);

G_END_DECLS

#endif /* __PDB_ARGS_H__ */
