/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>

#include "config.h"

#include "pdbaccumulator.h"

gboolean 
pdb_list_accumulator (GSignalInvocationHint *hint,
                      GValue                *return_accumulator,
                      const GValue          *handler_return,
                      gpointer               user_data)
{
  GSList *old_list, *new_list, *list;

  old_list = g_value_get_pointer (return_accumulator);
  new_list = g_value_get_pointer (handler_return);
  
  list = g_slist_concat (old_list, new_list);

  g_value_set_pointer (return_accumulator, list);

  return TRUE;
}

gboolean
pdb_stop_on_true_accumulator (GSignalInvocationHint *hint,
                              GValue                *return_accumulator,
                              const GValue          *handler_return,
                              gpointer               user_data)
{
  gboolean handled;


  handled = g_value_get_boolean (handler_return);

  g_value_set_boolean (return_accumulator, handled);
  
  return ! handled;
}

gboolean
pdb_object_accumulator (GSignalInvocationHint *hint,
                        GValue                *return_accumulator,
                        const GValue          *handler_return,
                        gpointer               user_data)
{
  GObject *obj;


  obj = g_value_get_object (handler_return);

  if (obj)
    g_value_set_object (return_accumulator, obj);
  
  return obj == NULL;
}
