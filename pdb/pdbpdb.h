/* LIBPDB - The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_PDB_H__
#define __PDB_PDB_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define PDB_TYPE_PDB            (pdb_get_type ())
#define PDB(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), PDB_TYPE_PDB, Pdb))
#define PDB_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  PDB_TYPE_PDB, PdbClass))
#define PDB_IS_PDB(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PDB_TYPE_PDB))
#define PDB_IS_PDB_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PDB_TYPE_PDB))
#define PDB_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  PDB_TYPE_PDB, PdbClass))

struct _Pdb {
  GObject     parent_instance;

  GHashTable *table;
};


struct _PdbClass {
  GObjectClass  parent_class;

  /* signals */
  PdbProc *  (* lookup_proc)        (Pdb          *pdb,
                                    const gchar  *name,
                                    gpointer      user_data);
  gboolean   (* register_proc)     (Pdb          *pdb, /* note: ref PdbProc */
                                    PdbProc      *proc,
                                    gpointer      user_data);
  void       (* proc_registered)   (Pdb          *pdb,
                                    PdbProc      *proc,
                                    gpointer      user_data);
  PdbProc *  (* unregister_proc)   (Pdb          *pdb,  /* note: do not unref PdbProc */
                                    const gchar  *name,
                                    gpointer      user_data);
  void       (* proc_unregistered) (Pdb          *pdb,
                                    PdbProc      *proc,
                                    gpointer      user_data);

  /* FIXME: pass arguments, return values */
  void       (* proc_executed)     (Pdb          *pdb,
                                    PdbProc      *proc);
};

typedef struct _PdbClass PdbClass;



/*  Functions  */
GType       pdb_get_type        (void) G_GNUC_CONST;
Pdb *       pdb_new             (void);
gboolean    pdb_register        (Pdb         *pdb,
                                 PdbProc     *procedure);
gboolean    pdb_unregister      (Pdb         *pdb,
                                 const gchar *name);
PdbProc *   pdb_lookup          (Pdb         *pdb, 
                                 const gchar *name);
PdbResult * pdb_execute_by_name (Pdb         *pdb, 
                                 const gchar *name,
                                 ...);

#if 0

const gchar * pdb_type_name (PdbArgType type); 

typedef enum
{
  PDB_EXECUTION_ERROR,
  PDB_CALLING_ERROR,
  PDB_PASS_THROUGH,
  PDB_SUCCESS,
  PDB_CANCEL
} PDBStatusType;
#endif


G_END_DECLS

#endif  /*  __PDB_PDB_H__  */
