/* LIBPDB - The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_TYPES_H__
#define __PDB_TYPES_H__

typedef struct _Pdb             Pdb;
typedef struct _PdbResult       PdbResult;
typedef struct _PdbProc         PdbProc;
typedef struct _PdbProcClass    PdbProcClass;
typedef struct _PdbArgs         PdbArgs;
typedef struct _PdbArgsClass    PdbArgsClass;

typedef enum 
{
        PDB_INT = 1,
        PDB_DOUBLE,
        PDB_STRING,
        PDB_POINTER
} PdbArgType;

#if 0
/*  Argument marshalling procedures  */
typedef PdbArgd * (* ArgMarshal) (PdbProc  *proc,
                                 PdbArgs   *args);
#endif

#endif /* __PDB_TYPES_H__ */


