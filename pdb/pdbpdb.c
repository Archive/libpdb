/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <stdarg.h>
#include <string.h>
#include <sys/types.h>

#include <glib.h>

#include "config.h"

#include "pdbtypes.h"

#include "pdbproc.h" 
#include "pdbargs.h"
#include "pdbpdb.h"

#include "pdbmarshal.h"
#include "pdbaccumulator.h"
#include "pdbdestroy.h"

#include "pdbintl.h" 
 
static void   pdb_class_init          (PdbClass   *klass); 
static void   pdb_init                (Pdb        *tool);
static void   pdb_finalize            (GObject    *object);

static GObjectClass *parent_class = NULL; 


enum {
  LOOKUP_PROC,
  REGISTER_PROC,
  PROC_REGISTERED,
  UNREGISTER_PROC,
  PROC_UNREGISTERED,
  PROC_EXECUTED,
  LAST_SIGNAL
};

static guint pdb_signals[LAST_SIGNAL] = { 0 };

static PdbProc * pdb_default_lookup     (Pdb           *pdb,
                                         const gchar   *name,
                                         gpointer       user_data);
static gboolean  pdb_default_register   (Pdb           *pdb,
                                         PdbProc       *proc,
                                         gpointer       user_data);
static PdbProc * pdb_default_unregister (Pdb           *pdb,
                                         const gchar   *name,
                                         gpointer       user_data);


GType
pdb_get_type (void)
{
  static GType pdb_type = 0;

  if (! pdb_type)
    {
      static const GTypeInfo pdb_info =
        {
          sizeof (PdbClass),
          (GBaseInitFunc) NULL,
          (GBaseFinalizeFunc) NULL,
          (GClassInitFunc) pdb_class_init,
          NULL,           /* class_finalize */
          NULL,           /* class_data     */
          sizeof (Pdb),
          0,              /* n_preallocs    */
          (GInstanceInitFunc) pdb_init,
        };

    pdb_type = g_type_register_static (G_TYPE_OBJECT,
                                       "Pdb", 
                                       &pdb_info, 0);
  }

  return pdb_type;
}

static void
pdb_init (Pdb *pdb)
{
  g_return_if_fail (pdb != NULL);
  g_return_if_fail (PDB_IS_PDB (pdb));

  pdb->table = g_hash_table_new_full (g_str_hash, g_str_equal, 
                                      NULL, pdb_destroy_object);
}

static void
pdb_class_init (PdbClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  parent_class = g_type_class_peek_parent (klass);
  object_class->finalize = pdb_finalize;

  klass->lookup_proc       = pdb_default_lookup;
  klass->register_proc     = pdb_default_register;
  klass->proc_registered   = NULL;
  klass->unregister_proc   = pdb_default_unregister;
  klass->proc_unregistered = NULL;
  klass->proc_executed     = NULL;

  pdb_signals[LOOKUP_PROC] = 
    g_signal_new ("lookup-proc",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, lookup_proc),
                  pdb_object_accumulator, NULL,
                  _pdb_marshal_OBJECT__STRING,
                  G_TYPE_OBJECT, 1,
                  G_TYPE_STRING);

  pdb_signals[REGISTER_PROC] = 
    g_signal_new ("register-proc",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, register_proc),
                  pdb_stop_on_true_accumulator, NULL,
                  _pdb_marshal_BOOLEAN__OBJECT,
                  G_TYPE_BOOLEAN, 1,
                  G_TYPE_OBJECT);

  pdb_signals[PROC_REGISTERED] = 
    g_signal_new ("proc-registered",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, proc_registered),
                  NULL, NULL,
                  _pdb_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1,
                  G_TYPE_OBJECT);
                  
  pdb_signals[UNREGISTER_PROC] = 
    g_signal_new ("unregister-proc",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, unregister_proc),
                  pdb_object_accumulator, NULL,
                  _pdb_marshal_OBJECT__STRING,
                  G_TYPE_OBJECT, 1,
                  G_TYPE_STRING);

  pdb_signals[PROC_UNREGISTERED] = 
    g_signal_new ("proc-unregistered",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, proc_unregistered),
                  NULL, NULL,
                  _pdb_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1,
                  G_TYPE_OBJECT);

  pdb_signals[PROC_EXECUTED] = 
    g_signal_new ("proc-executed",
                  G_OBJECT_CLASS_TYPE (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (PdbClass, proc_executed),
                  NULL, NULL,
                  _pdb_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1,
                  G_TYPE_OBJECT);
}


static void
pdb_finalize (GObject *object)
{
  Pdb *pdb;

  g_return_if_fail (PDB_IS_PDB (object));
  pdb = PDB (object);

  if (pdb->table)
    {
      /* emit unregistered using g_hash_table_foreach? -- Rockwalrus */
      g_hash_table_destroy (pdb->table);

      pdb->table = NULL;
    }

  parent_class->finalize (object);
}


/**
 * pdb_new:
 *
 * Creates a new, empty PDB. A program can have more than one PDB.
 *
 * Returns: a newly allocated #Pdb.
 */

Pdb *
pdb_new (void) {
  return g_object_new(PDB_TYPE_PDB, NULL);
}

/**
 * pdb_register:
 * @pdb: a #Pdb.
 * @procedure: the PdbProc representing the procedure to register.
 *
 * Registers a procedure in the #Pdb.
 *
 * Returns: TRUE if the procedure is registered sucessfully.
 */

gboolean
pdb_register (Pdb        *pdb,
              PdbProc    *procedure)
{
  gboolean success;

  g_return_val_if_fail (pdb != NULL, FALSE);
  g_return_val_if_fail (procedure != NULL, FALSE);

  g_return_val_if_fail (PDB_IS_PDB (pdb), FALSE);
  g_return_val_if_fail (PDB_IS_PROC (procedure), FALSE);

  g_signal_emit (G_OBJECT (pdb),
                 pdb_signals[REGISTER_PROC], 0,
                 procedure,
                 &success);

  if (!success)
    return FALSE;

  g_signal_emit (G_OBJECT (pdb),
                 pdb_signals[PROC_REGISTERED], 0,
                 procedure);

  return TRUE;
}

static gboolean
pdb_default_register (Pdb        *pdb,
                      PdbProc    *procedure,
                      gpointer    user_data)
 {
  g_return_val_if_fail (pdb != NULL, FALSE);
  g_return_val_if_fail (procedure != NULL, FALSE);
 
  g_return_val_if_fail (PDB_IS_PDB (pdb), FALSE);
  g_return_val_if_fail (PDB_IS_PROC (procedure), FALSE);
 
  g_object_ref (G_OBJECT (procedure)); /* our own copy */

  g_hash_table_replace (pdb->table,
                        (gpointer) procedure->name,
                        (gpointer) procedure);

  return TRUE;
 }
 
/**
 * pdb_unregister:
 * @pdb: a #Pdb.
 * @name: the name of the procedure to unregister.
 *
 * Removes a previously registered procedure from the #Pdb.
 *
 * Returns: TRUE if the procedure is unregistered succesfully.  Connect to the 
 * proc-unregistered signal if you need to know exactly which #PdbProc is
 * unregistered.
 */
 
gboolean
pdb_unregister (Pdb         *pdb,
                const gchar *name)
{
  PdbProc *proc;

  g_return_val_if_fail (pdb != NULL, FALSE);
  g_return_val_if_fail (name != NULL, FALSE);

  g_return_val_if_fail (PDB_IS_PDB (pdb), FALSE);

  g_signal_emit (G_OBJECT (pdb),
                 pdb_signals[UNREGISTER_PROC], 0,
                 name,
                 &proc);

  if (!proc)
    return FALSE;

  g_signal_emit (G_OBJECT (pdb),
                 pdb_signals[PROC_UNREGISTERED], 0,
                 proc);

  g_object_unref (G_OBJECT (proc));
  
  return TRUE;
}

static PdbProc *
pdb_default_unregister (Pdb         *pdb,
                        const gchar *name,
                        gpointer     user_data)
 {
  PdbProc *proc;
 
  g_return_val_if_fail (pdb != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);
 
  g_return_val_if_fail (PDB_IS_PDB (pdb), NULL);
 
  proc = g_hash_table_lookup (pdb->table, (gpointer) name);

  if (proc)
    g_hash_table_steal (pdb->table, (gpointer) name);
    
  return proc;
}

/**
 * pdb_lookup:
 * @pdb: a #Pdb.
 * @name: the name of the procedure to lookup.
 *
 * Looks up the #PdbProc associated with @name.
 *
 * Returns: a #PdbProc, or NULL on failure.
 */

PdbProc *
pdb_lookup (Pdb         *pdb,
            const gchar *name)
 {
  PdbProc *proc;
 
  g_return_val_if_fail (pdb != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);
 
  g_return_val_if_fail (PDB_IS_PDB (pdb), NULL);
 
  g_signal_emit (G_OBJECT (pdb),
                 pdb_signals[LOOKUP_PROC], 0,
                 name,
                 &proc);
  return proc;
 }
 
static PdbProc *
pdb_default_lookup (Pdb         *pdb,
                    const gchar *name,
                    gpointer     user_data)
{
  PdbProc *proc;

  g_return_val_if_fail (pdb != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);

  g_return_val_if_fail (PDB_IS_PDB (pdb), NULL);

  proc = g_hash_table_lookup (pdb->table, (gpointer) name);

  return proc;
}




/**
 * pdb_execute_by_name:
 * @pdb: a #Pdb.
 * @name: the name of the procedure to invoke.
 * @...: A list of arguments passed to the procedure of the form "name", value
 * 
 * Invokes the procedure specifed by @name with the arguments provided. The
 * result is stored in retval. If retval is NULL, the return value of the
 * procedure is ignored.
 *
 * Returns: the values returned by the procedure.
 */


PdbResult * 
pdb_execute_by_name (Pdb         *pdb,
                     const gchar *name,
                     ...)

{
  PdbProc   *proc;
  PdbArgs   *args;
  PdbResult *result;
  va_list    va;

  g_return_val_if_fail (pdb != NULL, NULL);
  g_return_val_if_fail (PDB_IS_PDB (pdb), NULL);

  /* The proc */
  proc = pdb_lookup (pdb, name);
  g_return_val_if_fail(proc != NULL, NULL);  /* FIXME: give real error message */

  /* The arguments */
  args = pdb_args_new();
  
  va_start(va, name);
  pdb_args_add_valist(args, proc, va);
  va_end(va);

  /* execute it */
  result = pdb_proc_exec(proc, args);

  g_object_unref(G_OBJECT(args));

  return result;
}

