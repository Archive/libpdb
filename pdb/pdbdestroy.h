/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2003 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_DESTROY_H__
#define __PDB_DESTROY_H__

G_BEGIN_DECLS

void pdb_destroy_object (gpointer object);
void pdb_destroy_value  (gpointer value);
void pdb_destroy_param_spec (gpointer param_spec);

G_END_DECLS

#endif /*  __PDB_DESTROY_H__ */
