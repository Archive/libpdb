<INCLUDE>pdb.h</INCLUDE>
<SECTION>
<FILE>pdbpdb</FILE>
<TITLE>Pdb</TITLE>
Pdb
pdb_new
pdb_register
pdb_unregister
pdb_lookup
pdb_execute
pdb_run_proc
pdb_return_args
pdb_destroy_args
pdb_type_name
PDBStatusType
<SUBSECTION Standard>
PDB
PDB_CLASS
PDB_GET_CLASS
PDB_IS_PDB
PDB_TYPE_PDB
PDB_IS_PDB_CLASS
pdb_get_type
</SECTION>

<SECTION>
<FILE>pdbproc</FILE>
<TITLE>PdbProc</TITLE>
PdbProc
PdbProcArg
PdbArg
pdb_proc_exec
pdb_proc_set_everything
IntExec
PlugInExec
ExtExec
TempExec
NetExec
PdbArgType
PDBProcType
<SUBSECTION Standard>
PDB_PROC
PDB_IS_PROC
PDB_TYPE_PROC
pdb_proc_get_type
PDB_PROC_CLASS
PDB_IS_PROC_CLASS
PDB_PROC_GET_CLASS
</SECTION>

<SECTION>
<FILE>pdbinternalproc</FILE>
<TITLE>PdbInternalProc</TITLE>
PdbInternalProc
pdb_internal_proc_new
<SUBSECTION Standard>
PDB_INTERNAL_PROC
PDB_IS_INTERNAL_PROC
PDB_TYPE_INTERNAL_PROC
pdb_internal_proc_get_type
PDB_INTERNAL_PROC_CLASS
PDB_IS_INTERNAL_PROC_CLASS
PDB_INTERNAL_PROC_GET_CLASS
</SECTION>

<SECTION>
<FILE>pdbtypes</FILE>
<TITLE>Misc. PDB Types</TITLE>
ArgMarshal
</SECTION>

<SECTION>
<FILE>gimpcruft</FILE>
<TITLE>Cruft Temporarily Here From GIMP</TITLE>
GimpParasite
GimpRGB
GimpHSV
GimpRenderFunc
GimpPutPixelFunc
GimpProgressFunc
</SECTION>
