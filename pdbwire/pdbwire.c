/* LIBPDBWIRE - The Procedural Database IPC Library
 * Copyright (C) 1995-2002 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>

#include <glib.h>


#include "pdbwire.h"


static gboolean _pdbwire_quit_read                (WireStdProtocol     *wsp,
                                                   Wire                *wire,
        				           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_quit_write               (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_quit_destroy             (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

#ifdef GIMP_CRUFT

static gboolean _pdbwire_config_read              (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_config_write             (WireStdProtocol     *wsp,
                                                   Wire                *wire,

					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_config_destroy           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

static gboolean _pdbwire_tile_req_read            (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_req_write           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_req_destroy         (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

static gboolean _pdbwire_tile_ack_read            (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_ack_write           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_ack_destroy         (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_data_read           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_data_write          (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_tile_data_destroy        (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
#endif

static gboolean _pdbwire_proc_run_read            (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_run_write           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_run_destroy         (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_return_read         (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_return_write        (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_return_destroy      (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

static gboolean _pdbwire_temp_proc_run_read       (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_temp_proc_run_write      (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_temp_proc_run_destroy    (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_temp_proc_return_read    (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_temp_proc_return_write   (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_temp_proc_return_destroy (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_install_read        (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_install_write       (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_install_destroy     (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_uninstall_read      (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_uninstall_write     (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_proc_uninstall_destroy   (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_extension_ack_read       (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_extension_ack_write      (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_extension_ack_destroy    (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

static gboolean _pdbwire_has_init_read            (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_has_init_write           (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           WireStdProtocolMsg  *msg,
                                                   GError             **error);
static gboolean _pdbwire_has_init_destroy         (WireStdProtocol     *wsp,
                                                   Wire                *wire,
                                                   WireStdProtocolMsg  *msg,
                                                   GError             **error);

static gboolean _pdbwire_params_read              (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           PdbWireParam       **params,
					           guint               *nparams,
                                                   GError             **error);
static gboolean _pdbwire_params_write             (WireStdProtocol     *wsp,
                                                   Wire                *wire,
					           PdbWireParam        *params,
					           gint                 nparams,
                                                   GError             **error);

static gboolean _pdbwire_params_destroy           (PdbWireParam        *params,
					           gint                 nparams);



void
pdbwire_register (WireStdProtocol *wsp)
{
  g_return_if_fail (wsp != NULL);
  g_return_if_fail (WIRE_IS_STD_PROTOCOL (wsp));

  wire_std_protocol_register_msg (wsp, PDBWIRE_QUIT,
		 _pdbwire_quit_read,
		 _pdbwire_quit_write,
		 _pdbwire_quit_destroy);
#ifdef GIMP_CRUFT
  wire_std_protocol_register_msg (wsp, PDBWIRE_CONFIG,
		 _pdbwire_config_read,
		 _pdbwire_config_write,
		 _pdbwire_config_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_TILE_REQ,
		 _pdbwire_tile_req_read,
		 _pdbwire_tile_req_write,
		 _pdbwire_tile_req_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_TILE_ACK,
		 _pdbwire_tile_ack_read,
		 _pdbwire_tile_ack_write,
		 _pdbwire_tile_ack_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_TILE_DATA,
		 _pdbwire_tile_data_read,
		 _pdbwire_tile_data_write,
		 _pdbwire_tile_data_destroy);

#endif
		 
  wire_std_protocol_register_msg (wsp, PDBWIRE_PROC_RUN,
		 _pdbwire_proc_run_read,
		 _pdbwire_proc_run_write,
		 _pdbwire_proc_run_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_PROC_RETURN,
		 _pdbwire_proc_return_read,
		 _pdbwire_proc_return_write,
		 _pdbwire_proc_return_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_TEMP_PROC_RUN,
		 _pdbwire_temp_proc_run_read,
		 _pdbwire_temp_proc_run_write,
		 _pdbwire_temp_proc_run_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_TEMP_PROC_RETURN,
		 _pdbwire_temp_proc_return_read,
		 _pdbwire_temp_proc_return_write,
		 _pdbwire_temp_proc_return_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_PROC_INSTALL,
		 _pdbwire_proc_install_read,
		 _pdbwire_proc_install_write,
		 _pdbwire_proc_install_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_PROC_UNINSTALL,
		 _pdbwire_proc_uninstall_read,
		 _pdbwire_proc_uninstall_write,
		 _pdbwire_proc_uninstall_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_EXTENSION_ACK,
		 _pdbwire_extension_ack_read,
		 _pdbwire_extension_ack_write,
		 _pdbwire_extension_ack_destroy);
  wire_std_protocol_register_msg (wsp, PDBWIRE_HAS_INIT,
		 _pdbwire_has_init_read,
		 _pdbwire_has_init_write,
		 _pdbwire_has_init_destroy);
}

gboolean
pdbwire_quit_write (WireStdProtocol  *wsp,
                    Wire             *wire,
                    GError          **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_QUIT;
  msg.data = NULL;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_config_write (WireStdProtocol  *wsp,
                      Wire             *wire,
      	              PdbWireConfig    *config,
      	              GError          **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_CONFIG;
  msg.data = config;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_tile_req_write (WireStdProtocol      *wsp,
                        Wire                 *wire,
		        PdbWireTileReq       *tile_req,
		        GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_TILE_REQ;
  msg.data = tile_req;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_tile_ack_write (WireStdProtocol      *wsp,
                        Wire                 *wire,
                        GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_TILE_ACK;
  msg.data = NULL;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_tile_data_write (WireStdProtocol      *wsp,
                         Wire                 *wire,
		         PdbWireTileData      *tile_data,
		         GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_TILE_DATA;
  msg.data = tile_data;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_proc_run_write (WireStdProtocol  *wsp,
                        Wire             *wire,
	   	        PdbWireProcRun   *proc_run,
	   	        GError          **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_PROC_RUN;
  msg.data = proc_run;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_proc_return_write (WireStdProtocol      *wsp,
                           Wire                 *wire,
 		           PdbWireProcReturn    *proc_return,
 		           GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_PROC_RETURN;
  msg.data = proc_return;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_temp_proc_run_write (WireStdProtocol      *wsp,
                             Wire                 *wire,
			     PdbWireProcRun       *proc_run,
			     GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_TEMP_PROC_RUN;
  msg.data = proc_run;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_temp_proc_return_write (WireStdProtocol      *wsp,
                                Wire                 *wire,
			        PdbWireProcReturn    *proc_return,
			        GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_TEMP_PROC_RETURN;
  msg.data = proc_return;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_proc_install_write (WireStdProtocol      *wsp,
                            Wire                 *wire,
		            PdbWireProcInstall   *proc_install,
		            GError              **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_PROC_INSTALL;
  msg.data = proc_install;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_proc_uninstall_write (WireStdProtocol       *wsp,
                              Wire                  *wire,
			      PdbWireProcUninstall  *proc_uninstall,
			      GError               **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_PROC_UNINSTALL;
  msg.data = proc_uninstall;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_extension_ack_write (WireStdProtocol *wsp,
                             Wire            *wire,
                             GError         **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_EXTENSION_ACK;
  msg.data = NULL;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

gboolean
pdbwire_has_init_write (WireStdProtocol  *wsp,
                        Wire             *wire,
                        GError          **error)
{
  WireStdProtocolMsg msg;

  msg.type = PDBWIRE_HAS_INIT;
  msg.data = NULL;

  if (! wire_std_protocol_write_msg (wsp, wire, &msg, error))
    return FALSE;
  if (! wire_flush (wire, error))
    return FALSE;
  return TRUE;
}

/*  quit  */

static gboolean
_pdbwire_quit_read (WireStdProtocol    *wsp,
                    Wire               *wire,
	            WireStdProtocolMsg *msg,
                    GError         **error)
{
  return TRUE;
}

static gboolean
_pdbwire_quit_write (WireStdProtocol    *wsp,
                     Wire               *wire,
 		     WireStdProtocolMsg *msg,
                    GError         **error)
{
  return TRUE;
}

static gboolean
_pdbwire_quit_destroy (WireStdProtocol    *wsp,
                       Wire               *wire,
                       WireStdProtocolMsg *msg,
                    GError         **error)
{
  return TRUE;
}


#ifdef GIMP_CRUFT
/*  config  */

static gboolean
_pdbwire_config_read (WireStdProtocol     *wsp,
                      Wire                *wire,
		      WireStdProtocolMsg  *msg,
                      GError             **error)
{
  PdbWireConfig *config;

  config = g_new (PdbWireConfig, 1);

  if (! wire_read_int32_v  (wire, &config->version, 1, error))
    goto cleanup;
  if (! wire_read_int32_v  (wire, &config->tile_width, 1, error))
    goto cleanup;
  if (! wire_read_int32_v  (wire, &config->tile_height, 1, error))
    goto cleanup;
  if (! wire_read_int32_v  (wire, (guint32 *) &config->shm_ID, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_double_v (wsp, wire, &config->gamma, 1, error))
    goto cleanup;
  if (! wire_read_int8_v   (wire, (guint8 *) &config->install_cmap, 1, error))
    goto cleanup;
  if (! wire_read_int8_v   (wire, (guint8 *) &config->unused, 1, error))
    goto cleanup;
  if (! wire_read_int32_v  (wire, (guint32 *) &config->min_colors, 1, error))
    goto cleanup;
  if (! wire_read_int32_v  (wire, (guint32 *) &config->gdisp_ID, 1, error))
    goto cleanup;

  msg->data = config;
  return TRUE;

 cleanup:
  g_free (config);
  return FALSE;
}

static gboolean
_pdbwire_config_write (WireStdProtocol     *wsp,
                       Wire                *wire,
		       WireStdProtocolMsg  *msg,
                       GError             **error)
{
  PdbWireConfig *config;

  config = msg->data;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->version, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->tile_width, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->tile_height, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->shm_ID, error))
    return FALSE;
  if (! wire_std_protocol_write_double (wsp, wire, config->gamma, error))
    return FALSE;
  if (! wire_std_protocol_write_int8 (wsp, wire, config->install_cmap, error))
    return FALSE;
  if (! wire_std_protocol_write_int8 (wsp, wire, config->unused, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->min_colors, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, config->gdisp_ID, error))
    return FALSE;

  return TRUE;
}

static gboolean
_pdbwire_config_destroy (WireStdProtocol     *wsp,
                         Wire                *wire,
                         WireStdProtocolMsg  *msg,
                         GError             **error)
{
  g_free (msg->data);

  return TRUE;
}

/*  tile_req  */

static gboolean
_pdbwire_tile_req_read (WireStdProtocol     *wsp,
                        Wire                *wire,
		        WireStdProtocolMsg  *msg,
                        GError             **error)
{
  PdbWireTileReq *tile_req;

  tile_req = g_new (PdbWireTileReq, 1);

  if (! wire_std_protocol_read_int32 (wsp, wire, &tile_req->drawable_ID, 1))
    goto cleanup;
  if (! wire_std_protocol_read_int32 (wsp, wire, &tile_req->tile_num, 1))
    goto cleanup;
  if (! wire_std_protocol_read_int32 (wsp, wire, &tile_req->shadow, 1))
    goto cleanup;

  msg->data = tile_req;
  return TRUE;

 cleanup:
  g_free (tile_req);

  return FALSE;
}

static gboolean
_pdbwire_tile_req_write (WireStdProtocol     *wsp,
                         Wire                *wire,
	                 WireStdProtocolMsg  *msg,
                         GError             **error)
{
  PdbWireTileReq *tile_req;

  tile_req = msg->data;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_req->drawable_ID, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_req->tile_num, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_req->shadow, error))
    return FALSE;

  return TRUE;
}

static gboolean
_pdbwire_tile_req_destroy (WireStdProtocol     *wsp,
                           Wire                *wire,
                           WireStdProtocolMsg  *msg,
                           GError             **error)
{
  g_free (msg->data);

  return TRUE;
}

/*  tile_ack  */

static gboolean
_pdbwire_tile_ack_read (WireStdProtocol     *wsp,
                        Wire                *wire,
		        WireStdProtocolMsg  *msg,
                        GError             **error)
{
  return TRUE;
}

static gboolean
_pdbwire_tile_ack_write (WireStdProtocol     *wsp,
                         Wire                *wire,
		         WireStdProtocolMsg  *msg,
                         GError             **error)
{
  return TRUE;
}

static gboolean
_pdbwire_tile_ack_destroy (WireStdProtocol     *wsp,
                           Wire                *wire,
                           WireStdProtocolMsg  *msg,
                           GError             **error)
{
  return TRUE;
}

/*  tile_data  */

static gboolean
_pdbwire_tile_data_read (WireStdProtocol     *wsp,
                         Wire                *wire,
 		         WireStdProtocolMsg  *msg,
                         GError             **error)
{
  PdbWireTileData *tile_data;
  guint length;

  tile_data = g_new0 (PdbWireTileData, 1);

  if (! wire_std_protocol_read_int32_v (wsp, wire, (guint32 *) &tile_data->drawable_ID, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->tile_num, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->shadow, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->bpp, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->width, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->height, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &tile_data->use_shm, 1, error))
    goto cleanup;

  if (!tile_data->use_shm)
    {
      length = tile_data->width * tile_data->height * tile_data->bpp;
      tile_data->data = g_new (guchar, length);

      if (! wire_std_protocol_read_int8_v (wsp, wire, (guint8 *) tile_data->data, length))
	goto cleanup;
    }

  msg->data = tile_data;
  return TRUE;

 cleanup:
  g_free (tile_data->data);
  g_free (tile_data);

  return FALSE;
}

static gboolean
_pdbwire_tile_data_write (WireStdProtocol     *wsp,
                          Wire                *wire,
		          WireStdProtocolMsg  *msg,
                          GError             **error)
{
  PdbWireTileData *tile_data;
  guint length;

  tile_data = msg->data;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->drawable_ID, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->tile_num, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->shadow, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->bpp, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->width, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->height, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, tile_data->use_shm, error))
    return FALSE;

  if (!tile_data->use_shm)
    {
      length = tile_data->width * tile_data->height * tile_data->bpp;
      if (! wire_std_protocol_write_int8 (wsp, wire, (guint8*) tile_data->data, length))
	return FALSE;
    }

  return TRUE;
}

static gboolean
_pdbwire_tile_data_destroy (WireStdProtocol     *wsp,
                            Wire                *wire,
                            WireStdProtocolMsg  *msg,
                            GError             **error)
{
  PdbWireTileData *tile_data;

  tile_data = msg->data;

  g_free (tile_data->data);
  g_free (tile_data);

  return TRUE;
}

#endif

/*  proc_run  */

static gboolean
_pdbwire_proc_run_read (WireStdProtocol     *wsp,
                        Wire                *wire,
  		        WireStdProtocolMsg  *msg,
                        GError             **error)
{
  PdbWireProcRun *proc_run;

  proc_run = g_new (PdbWireProcRun, 1);

  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_run->name, 1, error))
    goto cleanup;

  _pdbwire_params_read (wsp, wire,
                   &proc_run->params, (guint*) &proc_run->nparams, error);

  msg->data = proc_run;
  return TRUE;

 cleanup:
  g_free (proc_run);

  return FALSE;
}

static gboolean
_pdbwire_proc_run_write (WireStdProtocol     *wsp,
                         Wire                *wire,
		         WireStdProtocolMsg  *msg,
                         GError             **error)
{
  PdbWireProcRun *proc_run;

  proc_run = msg->data;

  if (! wire_std_protocol_write_string (wsp, wire, proc_run->name, error))
    return FALSE;

  return _pdbwire_params_write (wsp, wire, proc_run->params, proc_run->nparams, error);
}

static gboolean
_pdbwire_proc_run_destroy (WireStdProtocol     *wsp,
                           Wire                *wire,
                           WireStdProtocolMsg  *msg,
                           GError             **error)
{
  PdbWireProcRun *proc_run;
  gboolean success;

  proc_run = msg->data;
  success = _pdbwire_params_destroy (proc_run->params, proc_run->nparams);

  g_free (proc_run->name);
  g_free (proc_run);

  return success;
}

/*  proc_return  */

static gboolean
_pdbwire_proc_return_read (WireStdProtocol     *wsp,
                           Wire                *wire,
		           WireStdProtocolMsg  *msg,
                           GError             **error)
{
  PdbWireProcReturn *proc_return;

  proc_return = g_new (PdbWireProcReturn, 1);

  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_return->name, 1, error))
    goto cleanup;

  if (!_pdbwire_params_read (wsp, wire, &proc_return->params, 
                             (guint*) &proc_return->nparams, error))
    goto cleanup;

  msg->data = proc_return;
  return TRUE;

 cleanup:
  g_free (proc_return);

  return FALSE;
}

static gboolean
_pdbwire_proc_return_write (WireStdProtocol     *wsp,
                            Wire                *wire,
		            WireStdProtocolMsg  *msg,
                            GError             **error)
{
  PdbWireProcReturn *proc_return;

  proc_return = msg->data;

  if (! wire_std_protocol_write_string (wsp, wire, proc_return->name, error))
    return FALSE;

  return _pdbwire_params_write (wsp, wire, proc_return->params, proc_return->nparams, error);
}

static gboolean
_pdbwire_proc_return_destroy (WireStdProtocol     *wsp,
                              Wire                *wire,
                              WireStdProtocolMsg  *msg,
                              GError             **error)
{
  gboolean success;
  PdbWireProcReturn *proc_return;

  proc_return = msg->data;
  success = _pdbwire_params_destroy (proc_return->params, proc_return->nparams);

  g_free (proc_return->name);
  g_free (proc_return);

  return success;
}

/*  temp_proc_run  */

static gboolean
_pdbwire_temp_proc_run_read (WireStdProtocol     *wsp,
                             Wire                *wire,
			     WireStdProtocolMsg  *msg,
                             GError             **error)
{
  return _pdbwire_proc_run_read (wsp, wire, msg, error);
}

static gboolean
_pdbwire_temp_proc_run_write (WireStdProtocol     *wsp,
                              Wire                *wire,
			      WireStdProtocolMsg  *msg,
                              GError             **error)
{
  return _pdbwire_proc_run_write (wsp, wire, msg, error);
}

static gboolean
_pdbwire_temp_proc_run_destroy (WireStdProtocol     *wsp,
                                Wire                *wire,
                                WireStdProtocolMsg  *msg,
                                GError             **error)
{
  return _pdbwire_proc_run_destroy (wsp, wire, msg, error);
}

/*  temp_proc_return  */

static gboolean
_pdbwire_temp_proc_return_read (WireStdProtocol     *wsp,
                                Wire                *wire,
			        WireStdProtocolMsg  *msg,
                                GError             **error)
{
  return _pdbwire_proc_return_read (wsp, wire, msg, error);
}

static gboolean
_pdbwire_temp_proc_return_write (WireStdProtocol     *wsp,
                                 Wire                *wire,
			         WireStdProtocolMsg  *msg,
                                 GError             **error)
{
  return _pdbwire_proc_return_write (wsp, wire, msg, error);
}

static gboolean
_pdbwire_temp_proc_return_destroy (WireStdProtocol     *wsp,
                                   Wire                *wire,
                                   WireStdProtocolMsg  *msg,
                                   GError             **error)
{
  return _pdbwire_proc_return_destroy (wsp, wire, msg, error);
}

/*  proc_install  */

static gboolean
_pdbwire_proc_install_read (WireStdProtocol     *wsp,
                            Wire                *wire,
		            WireStdProtocolMsg  *msg,
                            GError             **error)
{
  PdbWireProcInstall *proc_install;
  gint i;

  proc_install = g_new0 (PdbWireProcInstall, 1);

  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->name, 1,  error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->blurb, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->help, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->author, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->copyright, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->date, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->menu_path, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_install->image_types, 1, error))
    goto cleanup;

  if (! wire_std_protocol_read_int32_v (wsp, wire, &proc_install->type, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &proc_install->nparams, 1, error))
    goto cleanup;
  if (! wire_std_protocol_read_int32_v (wsp, wire, &proc_install->nreturn_vals, 1, error))
    goto cleanup;

  proc_install->params = g_new0 (PdbWireParamDef, proc_install->nparams);

  for (i = 0; i < proc_install->nparams; i++)
    {
      if (! wire_std_protocol_read_int32_v (wsp, wire,
                              &proc_install->params[i].type, 1, error))
	goto cleanup;
      if (! wire_std_protocol_read_string_v (wsp, wire,
                              &proc_install->params[i].name, 1, error))
	goto cleanup;
      if (! wire_std_protocol_read_string_v (wsp, wire,
                              &proc_install->params[i].description, 1, error))
	goto cleanup;
    }

  proc_install->return_vals = g_new0 (PdbWireParamDef, proc_install->nreturn_vals);

  for (i = 0; i < proc_install->nreturn_vals; i++)
    {
      if (! wire_std_protocol_read_int32_v (wsp, wire,
                             (guint32*) &proc_install->return_vals[i].type, 1, error))
	goto cleanup;
      if (! wire_std_protocol_read_string_v (wsp, wire,
                              &proc_install->return_vals[i].name, 1, error))
	goto cleanup;
      if (! wire_std_protocol_read_string_v (wsp, wire,
                              &proc_install->return_vals[i].description, 1, error))
	goto cleanup;
    }

  msg->data = proc_install;
  return TRUE;

 cleanup:
  g_free (proc_install->name);
  g_free (proc_install->blurb);
  g_free (proc_install->help);
  g_free (proc_install->author);
  g_free (proc_install->copyright);
  g_free (proc_install->date);
  g_free (proc_install->menu_path);
  g_free (proc_install->image_types);

  if (proc_install->params)
    {
      for (i = 0; i < proc_install->nparams; i++)
	{
	  if (!proc_install->params[i].name)
	    break;

	  g_free (proc_install->params[i].name);
	  g_free (proc_install->params[i].description);
	}

      g_free (proc_install->params);
    }

  if (proc_install->return_vals)
    {
      for (i = 0; i < proc_install->nreturn_vals; i++)
	{
	  if (!proc_install->return_vals[i].name)
	    break;

	  g_free (proc_install->return_vals[i].name);
	  g_free (proc_install->return_vals[i].description);
	}

      g_free (proc_install->return_vals);
    }

  g_free (proc_install);

  return FALSE;
}

static gboolean
_pdbwire_proc_install_write (WireStdProtocol     *wsp,
                             Wire                *wire,
			     WireStdProtocolMsg  *msg,
			     GError             **error)
{
  PdbWireProcInstall *proc_install;
  gint i;

  proc_install = msg->data;

  if (! wire_std_protocol_write_string (wsp, wire, proc_install->name, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->blurb, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->help, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->author, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->copyright, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->date, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->menu_path, error))
    return FALSE;
  if (! wire_std_protocol_write_string (wsp, wire, proc_install->image_types, error))
    return FALSE;

  if (! wire_std_protocol_write_int32 (wsp, wire, proc_install->type, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, proc_install->nparams, error))
    return FALSE;
  if (! wire_std_protocol_write_int32 (wsp, wire, proc_install->nreturn_vals, error))
    return FALSE;

  for (i = 0; i < proc_install->nparams; i++)
    {
      if (! wire_std_protocol_write_int32 (wsp, wire,
                                           proc_install->params[i].type, error))
	return FALSE;
      if (! wire_std_protocol_write_string (wsp, wire,
                                            proc_install->params[i].name, error))
	return FALSE;
      if (! wire_std_protocol_write_string (wsp, wire,
                                            proc_install->params[i].description, 
                                            error))
	return FALSE;
    }

  for (i = 0; i < proc_install->nreturn_vals; i++)
    {
      if (! wire_std_protocol_write_int32 (wsp, wire,
                                           proc_install->return_vals[i].type, 
                                           error))
	return FALSE;
      if (! wire_std_protocol_write_string (wsp, wire,
                                            proc_install->return_vals[i].name, 
                                            error))
	return FALSE;
      if (! wire_std_protocol_write_string (wsp, wire,
                                            proc_install->return_vals[i].description, 
                                            error))
	return FALSE;
    }

  return TRUE;
}

static gboolean
_pdbwire_proc_install_destroy (WireStdProtocol    *wsp,
                               Wire               *wire,
                               WireStdProtocolMsg *msg,
                    GError         **error)
{
  PdbWireProcInstall *proc_install;
  gint i;

  proc_install = msg->data;

  g_free (proc_install->name);
  g_free (proc_install->blurb);
  g_free (proc_install->help);
  g_free (proc_install->author);
  g_free (proc_install->copyright);
  g_free (proc_install->date);
  g_free (proc_install->menu_path);
  g_free (proc_install->image_types);

  for (i = 0; i < proc_install->nparams; i++)
    {
      g_free (proc_install->params[i].name);
      g_free (proc_install->params[i].description);
    }

  for (i = 0; i < proc_install->nreturn_vals; i++)
    {
      g_free (proc_install->return_vals[i].name);
      g_free (proc_install->return_vals[i].description);
    }

  g_free (proc_install->params);
  g_free (proc_install->return_vals);
  g_free (proc_install);

  return FALSE;
}

/*  proc_uninstall  */

static gboolean
_pdbwire_proc_uninstall_read (WireStdProtocol     *wsp,
                              Wire                *wire,
			      WireStdProtocolMsg  *msg,
			      GError             **error)
{
  PdbWireProcUninstall *proc_uninstall;

  proc_uninstall = g_new (PdbWireProcUninstall, 1);

  if (! wire_std_protocol_read_string_v (wsp, wire, &proc_uninstall->name, 1, error))
    goto cleanup;

  msg->data = proc_uninstall;
  return TRUE;

 cleanup:
  g_free (proc_uninstall);

  return FALSE;
}

static gboolean
_pdbwire_proc_uninstall_write (WireStdProtocol    *wsp,
                               Wire               *wire,
                               WireStdProtocolMsg *msg,
                               GError            **error)
{
  PdbWireProcUninstall *proc_uninstall;

  proc_uninstall = msg->data;

  if (! wire_std_protocol_write_string (wsp, wire, proc_uninstall->name, error))
    return FALSE;

  return TRUE;
}

static gboolean
_pdbwire_proc_uninstall_destroy (WireStdProtocol     *wsp,
                                 Wire                *wire,
                                 WireStdProtocolMsg  *msg,
                                 GError             **error)
{
  PdbWireProcUninstall *proc_uninstall;

  proc_uninstall = msg->data;

  g_free (proc_uninstall->name);
  g_free (proc_uninstall);

  return TRUE;
}

/*  extension_ack  */

static gboolean
_pdbwire_extension_ack_read (WireStdProtocol     *wsp,
                             Wire                *wire,
			     WireStdProtocolMsg  *msg,
                             GError             **error)
{
  return TRUE;
}

static gboolean
_pdbwire_extension_ack_write (WireStdProtocol    *wsp,
                              Wire               *wire,
			      WireStdProtocolMsg *msg,
                    GError         **error)
{
  return TRUE;
}

static gboolean
_pdbwire_extension_ack_destroy (WireStdProtocol    *wsp,
                                Wire               *wire,
                                WireStdProtocolMsg *msg,
                    GError         **error)
{
  return TRUE;
}

/*  params  */

static gboolean
_pdbwire_params_read (WireStdProtocol  *wsp,
                      Wire             *wire,
		      PdbWireParam    **params,
		      guint            *nparams,
		      GError          **error)
{
  gint i, j;

  if (! wire_std_protocol_read_int32_v (wsp, wire, (guint32 *) nparams, 1, error))
    return FALSE;

  if (*nparams == 0)
    {
      *params = NULL;
      return TRUE;
    }

  *params = g_new (PdbWireParam, *nparams);

  for (i = 0; i < *nparams; i++)
    {
      if (! wire_std_protocol_read_int32_v (wsp, wire,
                             (guint32*) &(*params)[i].type, 1, error))
	goto cleanup;

      switch ((*params)[i].type)
	{
	case PDB_INT32:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_int32, 1, error))
	    goto cleanup;
	  break;

	case PDB_INT16:
	  if (! wire_std_protocol_read_int16_v (wsp, wire,
                                 (guint16*) &(*params)[i].data.d_int16, 1, error))
	    goto cleanup;
	  break;

	case PDB_INT8:
	  if (! wire_std_protocol_read_int8_v (wsp, wire,
                                (guint8*) &(*params)[i].data.d_int8, 1, error))
	    goto cleanup;
	  break;

        case PDB_FLOAT:
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  &(*params)[i].data.d_float, 1, error))
	    goto cleanup;
          break;

        case PDB_STRING:
	  if (! wire_std_protocol_read_string_v (wsp, wire,
                                  &(*params)[i].data.d_string, 1, error))
	    goto cleanup;
          break;

        case PDB_INT32ARRAY:
	  (*params)[i].data.d_int32array =
	    g_new (gint32, (*params)[i-1].data.d_int32);
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) (*params)[i].data.d_int32array,
                                 (*params)[i-1].data.d_int32,
                                 error))
	    {
	      g_free ((*params)[i].data.d_int32array);
	      goto cleanup;
	    }
          break;

        case PDB_INT16ARRAY:
	  (*params)[i].data.d_int16array =
	    g_new (gint16, (*params)[i-1].data.d_int32);
	  if (! wire_std_protocol_read_int16_v (wsp, wire,
                                 (guint16*) (*params)[i].data.d_int16array,
                                 (*params)[i-1].data.d_int32,
                                 error))
	    {
	      g_free ((*params)[i].data.d_int16array);
	      goto cleanup;
	    }
          break;

        case PDB_INT8ARRAY:
	  (*params)[i].data.d_int8array =
	    g_new (gint8, (*params)[i-1].data.d_int32);
	  if (! wire_std_protocol_read_int8_v (wsp, wire,
                                (guint8*) (*params)[i].data.d_int8array,
                                (*params)[i-1].data.d_int32,
                                error))
	    {
	      g_free ((*params)[i].data.d_int8array);
	      goto cleanup;
	    }
          break;

        case PDB_FLOATARRAY:
	  (*params)[i].data.d_floatarray =
	    g_new (gdouble, (*params)[i-1].data.d_int32);
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  (*params)[i].data.d_floatarray,
                                  (*params)[i-1].data.d_int32,
                                  error))
	    {
	      g_free ((*params)[i].data.d_floatarray);
	      goto cleanup;
	    }
          break;

        case PDB_STRINGARRAY:
	  (*params)[i].data.d_stringarray =
	    g_new0 (gchar*, (*params)[i-1].data.d_int32);
	  if (! wire_std_protocol_read_string_v (wsp, wire,
                                  (*params)[i].data.d_stringarray,
                                  (*params)[i-1].data.d_int32,
                                  error))
	    {
	      for (j = 0; j < (*params)[i-1].data.d_int32; j++)
		g_free (((*params)[i].data.d_stringarray)[j]);
	      g_free ((*params)[i].data.d_stringarray);
	      goto cleanup;
	    }
          break;

#ifdef GIMP_CRUFT
        case PDB_COLOR:
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  &(*params)[i].data.d_color.r, 1, error))
	    goto cleanup;
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  &(*params)[i].data.d_color.g, 1, error))
	    goto cleanup;
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  &(*params)[i].data.d_color.b, 1, error))
	    goto cleanup;
	  if (! wire_std_protocol_read_double_v (wsp, wire,
                                  &(*params)[i].data.d_color.a, 1, error))
	    goto cleanup;
	  break;

        case PDB_REGION:
          break;

        case PDB_DISPLAY:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_display, 1, error))
	    goto cleanup;
          break;

        case PDB_IMAGE:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_image, 1, error))
	    goto cleanup;
          break;

        case PDB_LAYER:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_layer, 1, error))
	    goto cleanup;
          break;

        case PDB_CHANNEL:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_channel, 1, error))
	    goto cleanup;
          break;

        case PDB_DRAWABLE:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_drawable, 1, error))
	    goto cleanup;
          break;

        case PDB_SELECTION:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_selection, 1, error))
	    goto cleanup;
          break;

        case PDB_BOUNDARY:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_boundary, 1, error))
	    goto cleanup;
          break;

        case PDB_PATH:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_path, 1, error))
	    goto cleanup;
          break;

        case PDB_PARASITE:
	  if (! wire_std_protocol_read_string_v (wsp, wire,
                                  &(*params)[i].data.d_parasite.name, 1, error))
	    goto cleanup;
	  if ((*params)[i].data.d_parasite.name == NULL)
	    {
	      /* we have a null parasite */
	      (*params)[i].data.d_parasite.data = NULL;
	      break;
	    }
	  if (! wire_std_protocol_read_int32 (wsp, wire,
                                 &((*params)[i].data.d_parasite.flags), 1, error))
	    goto cleanup;
	  if (! wire_std_protocol_read_int32 (wsp, wire,
                                 &((*params)[i].data.d_parasite.size), 1, error))
	    goto cleanup;
	  if ((*params)[i].data.d_parasite.size > 0)
	    {
	      (*params)[i].data.d_parasite.data = g_malloc ((*params)[i].data.d_parasite.size);
	      if (! wire_std_protocol_read_int8 (wsp, wire,
                                    (*params)[i].data.d_parasite.data,
                                    (*params)[i].data.d_parasite.size))
		{
		  g_free ((*params)[i].data.d_parasite.data);
		  goto cleanup;
		}
	    }
	  else
	    (*params)[i].data.d_parasite.data = NULL;
	  break;
#endif /* GIMP_CRUFT */

        case PDB_STATUS:
	  if (! wire_std_protocol_read_int32_v (wsp, wire,
                                 (guint32*) &(*params)[i].data.d_status, 1, error))
	    goto cleanup;
          break;

	case PDB_END:
	  break;
	}
    }

  return TRUE;

 cleanup:
  *nparams = 0;
  g_free (*params);
  *params = NULL;

  return FALSE;
}

static gboolean
_pdbwire_params_write (WireStdProtocol *wsp,
                       Wire            *wire,
		       PdbWireParam    *params,
		       gint             nparams,
		       GError         **error)
{
  gint i;

  if (! wire_std_protocol_write_int32 (wsp, wire, nparams, error))
    return FALSE;

  for (i = 0; i < nparams; i++)
    {
      if (! wire_std_protocol_write_int32 (wsp, wire,
                                           params[i].type, error))
	return FALSE;

      switch (params[i].type)
	{
	case PDB_INT32:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_int32, 
                                               error))
	    return FALSE;
	  break;

	case PDB_INT16:
	  if (! wire_std_protocol_write_int16 (wsp, wire,
                                               params[i].data.d_int16, 
                                               error))
	    return FALSE;
	  break;

	case PDB_INT8:
	  if (! wire_std_protocol_write_int8 (wsp, wire,
                                              params[i].data.d_int8, 
                                              error))
	    return FALSE;
	  break;

        case PDB_FLOAT:
	  if (! wire_std_protocol_write_double (wsp, wire,
                                                params[i].data.d_float, 
                                                error))
	    return FALSE;
          break;

        case PDB_STRING:
	  if (! wire_std_protocol_write_string (wsp, wire,
                                                params[i].data.d_string,
                                                error))
	    return FALSE;
          break;

        case PDB_INT32ARRAY:
	  if (! wire_std_protocol_write_int32_v (wsp, wire,
                                                 params[i].data.d_int32array,
                                                 params[i-1].data.d_int32,
                                                 error))
	    return FALSE;
          break;

        case PDB_INT16ARRAY:
	  if (! wire_std_protocol_write_int16_v (wsp, wire,
                                                 params[i].data.d_int16array,
                                                 params[i-1].data.d_int32,
                                                 error))
	    return FALSE;
          break;

        case PDB_INT8ARRAY:
	  if (! wire_std_protocol_write_int8_v (wsp, wire,
                                                params[i].data.d_int8array,
                                                params[i-1].data.d_int32,
                                                error))
	    return FALSE;
          break;

        case PDB_FLOATARRAY:
	  if (! wire_std_protocol_write_double_v (wsp, wire,
                                                  params[i].data.d_floatarray,
                                                  params[i-1].data.d_int32,
                                                  error))
	    return FALSE;
          break;

        case PDB_STRINGARRAY:
	  if (! wire_std_protocol_write_string_v (wsp, wire,
                                                  params[i].data.d_stringarray,
                                                  params[i-1].data.d_int32,
                                                  error))
	    return FALSE;
          break;

#ifdef GIMP_CRUFT
        case PDB_COLOR:
	  {
	    GimpRGB *color = (GimpRGB *) &params[i].data.d_color;
	    if (! wire_std_protocol_write_double (wsp, wire, &color->r, error))
	      return FALSE;
	    if (! wire_std_protocol_write_double (wsp, wire, &color->g, error))
	      return FALSE;
	    if (! wire_std_protocol_write_double (wsp, wire, &color->b, error))
	      return FALSE;
	    if (! wire_std_protocol_write_double (wsp, wire, &color->a, error))
	      return FALSE;
	  }
          break;

        case PDB_REGION:
          break;

        case PDB_DISPLAY:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_display, error))
	    return FALSE;
          break;

        case PDB_IMAGE:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_image, error))
	    return FALSE;
          break;

        case PDB_LAYER:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_layer, error))
	    return FALSE;
          break;

        case PDB_CHANNEL:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_error))
	    return FALSE;
          break;

        case PDB_DRAWABLE:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_drawable, error))
	    return FALSE;
          break;

        case PDB_SELECTION:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_selection, error))
	    return FALSE;
          break;

        case PDB_BOUNDARY:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_boundary, error))
	    return FALSE;
          break;

        case PDB_PATH:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_path, error))
	    return FALSE;
          break;

        case PDB_PARASITE:
	  {
	    GimpParasite *p = (GimpParasite *) &params[i].data.d_parasite;
	    if (p->name == NULL)
	      {
		/* write a null string to signifly a null parasite */
		wire_std_protocol_write_string (wsp, wire,  p->name, error);
		break;
	      }
	    if (! wire_std_protocol_write_string (wsp, wire, p->name, error))
	      return FALSE;
	    if (! wire_std_protocol_write_int32 (wsp, wire, p->flags, error))
	      return FALSE;
	    if (! wire_std_protocol_write_int32 (wsp, wire, p->size, error))
	      return FALSE;
	    if (p->size > 0)
	      {
		if (! wire_std_protocol_write_int8_v (wsp, wire, p->data, p->size, error))
		  return FALSE;
	      }
	  }
	  break;
#endif /* GIMP_CRUFT */

        case PDB_STATUS:
	  if (! wire_std_protocol_write_int32 (wsp, wire,
                                               params[i].data.d_status, error))
	    return FALSE;
          break;

	case PDB_END:
	  break;
	}
    }
    
  return TRUE;
}

static gboolean
_pdbwire_params_destroy (PdbWireParam    *params,
 		         gint             nparams)
{
  gint count;
  gint i, j;

  for (i = 0; i < nparams; i++)
    {
      switch (params[i].type)
	{
	case PDB_INT32:
	case PDB_INT16:
	case PDB_INT8:
	case PDB_FLOAT:
#ifdef GIMP_CRUFT
	case PDB_COLOR:
	case PDB_REGION:
	case PDB_DISPLAY:
	case PDB_IMAGE:
	case PDB_LAYER:
	case PDB_CHANNEL:
	case PDB_DRAWABLE:
	case PDB_SELECTION:
	case PDB_BOUNDARY:
	case PDB_PATH:
#endif
	case PDB_STATUS:
	  break;

	case PDB_STRING:
	  g_free (params[i].data.d_string);
	  break;

	case PDB_INT32ARRAY:
	  g_free (params[i].data.d_int32array);
	  break;

	case PDB_INT16ARRAY:
	  g_free (params[i].data.d_int16array);
	  break;

	case PDB_INT8ARRAY:
	  g_free (params[i].data.d_int8array);
	  break;

	case PDB_FLOATARRAY:
	  g_free (params[i].data.d_floatarray);
	  break;

	case PDB_STRINGARRAY:
	  if ((i > 0) && (params[i-1].type == PDB_INT32))
	    {
	      count = params[i-1].data.d_int32;
	      for (j = 0; j < count; j++)
		g_free (params[i].data.d_stringarray[j]);
	      g_free (params[i].data.d_stringarray);
	    }
	  break;

#ifdef GIMP_CRUFT
	case PDB_PARASITE:
	  if (params[i].data.d_parasite.name)
	    g_free(params[i].data.d_parasite.name);
	  if (params[i].data.d_parasite.data)
	    g_free(params[i].data.d_parasite.data);
	  break;

#endif

	case PDB_END:
	  break;
	}
    }

  g_free (params);

  return TRUE;
}

/* has_init */

static gboolean
_pdbwire_has_init_read (WireStdProtocol     *wsp,
                        Wire                *wire,
   	                WireStdProtocolMsg  *msg,
                        GError             **error)
{
  return TRUE;
}

static gboolean
_pdbwire_has_init_write (WireStdProtocol     *wsp,
                         Wire                *wire,
 	     	         WireStdProtocolMsg  *msg,
                         GError             **error)
{
  return TRUE;
}

static gboolean
_pdbwire_has_init_destroy (WireStdProtocol     *wsp,
                           Wire                *wire,
                           WireStdProtocolMsg  *msg,
                           GError             **error)
{
  return TRUE;
}

