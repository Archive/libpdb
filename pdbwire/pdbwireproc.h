/* LIBPDB - The Procedural Database Library
 * Copyright (C) 1995-2002 Spencer Kimball, Peter Mattis and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDB_WIRE_PROC_H__
#define __PDB_WIRE_PROC_H__

#include "pdbproc.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define PDB_TYPE_WIRE_PROC            (pdb_wire_proc_get_type ())
#define PDB_WIRE_PROC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PDB_TYPE_WIRE_PROC, PdbWireProc))
#define PDB_WIRE_PROC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  PDB_TYPE_WIRE_PROC, PdbWireProcClass))
#define PDB_IS_WIRE_PROC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PDB_TYPE_WIRE_PROC))
#define PDB_IS_WIRE_PROC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  PDB_TYPE_WIRE_PROC))
#define PDB_WIRE_PROC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  PDB_TYPE_WIRE_PROC, PdbWireProcClass))


/*  Structure for an wire procedure  */
struct _PdbWireProc {
  PdbProc          parent_instance;

  Wire            *wire;
  WireStdProtocol *protocol;
};

typedef struct _PdbWireProc PdbWireProc;

struct _PdbWireProcClass {
  PdbProcClass  parent_class;

};

typedef struct _PdbWireProcClass PdbWireProcClass;

GType      pdb_wire_proc_get_type  (void) G_GNUC_CONST;
PdbProc  * pdb_wire_proc_new       (gchar           *name,
                                    gchar           *blurb,
                                    gchar           *help,
                                    gchar           *version,
                                    gchar           *author,
                                    gchar           *copyright,
                                    gchar           *date,
                                    gint32           num_args,
                                    PdbProcArg      *args,
                                    gint32           num_values,
                                    PdbProcArg      *values,
                                    Wire            *wire,
                                    WireStdProtocol *protocol);
G_END_DECLS

#endif  /*  __PDB_WIRE_PROC_H__  */
