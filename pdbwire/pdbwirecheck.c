#include <glib.h>
#include "pdbwire.h"

int main (int argc, char **argv) {
  WireStdProtocol *wsp;

  g_type_init();
  
  wsp = (WireStdProtocol *) wire_std_protocol_new(TRUE);

  pdbwire_register(wsp);

  return 0;
}
