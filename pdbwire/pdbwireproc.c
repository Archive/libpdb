/* LIBPDB -- The Procedural Database Library
 * Copyright (C) 1995-2002 Spencer Kimball, Peter Mattis, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <stdarg.h>
#include <string.h>
#include <sys/types.h>

#include <glib.h>

#include "pdbwire.h"
#include "pdbwireproc.h"

static void      pdb_wire_proc_class_init (PdbProcClass  *klass); 
static PdbArg  * pdb_wire_proc_exec       (PdbProc       *proc,
                                           PdbArg        *args);

GType
pdb_wire_proc_get_type (void)
{
  static GType pdb_wire_proc_type = 0;

  if (! pdb_wire_proc_type)
    {
      static const GTypeInfo pdb_wire_proc_info =
      {
        sizeof (PdbWireProcClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) pdb_wire_proc_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (PdbWireProc),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) NULL,
      };

      pdb_wire_proc_type = g_type_register_static (PDB_TYPE_PROC,
	                                           "PdbWireProc", 
                                                   &pdb_wire_proc_info, 0);
    }

  return pdb_wire_proc_type;
}

static void
pdb_wire_proc_class_init (PdbProcClass *klass)
{
  klass->exec = pdb_wire_proc_exec;

}


static PdbArg *
pdb_wire_proc_exec  (PdbProc  *proc,
                     PdbArg   *args)
{
  PdbWireProc *wproc;

  g_return_val_if_fail (proc != NULL, NULL);
  g_return_val_if_fail (PDB_IS_WIRE_PROC(proc), NULL);

  wproc = PDB_WIRE_PROC (proc);

  /* FIXME!!! 

  pdbwire_proc_run_write (wproc->protocol,
                          wproc->wire,
                          ...)

  // Check for errors //

  return pdbwire_proc_return_read (wproc->protocol,
                                   wproc->wire,
                                   ...)
  */

  return NULL;

}

/**
 * pdb_wire_proc_new:
 * @name: The name of the procedure.
 * @blurb: A short description of the procedure.
 * @help: A longer description of the procedure.
 * @version: the version of the procedure.
 * @author: the author of the procedure.
 * @copyright: the copyright owner of the procedure.
 * @date: the date the procedure was written.
 * @num_args: the number of arguments.
 * @args: the arguments.
 * @num_values: the number of return values (do NOT include the status code in 
 * this count.)
 * @values: the values returned.
 * @proc: The funciton to be called.
 *
 * Creates a new #PdbWireProc with the supplied charactoristics.
 *
 * Returns: a new #PdbWireProc.
 */

PdbProc *
pdb_wire_proc_new (gchar           *name,
                   gchar           *blurb,
                   gchar           *help,
                   gchar           *version,
                   gchar           *author,
                   gchar           *copyright,
                   gchar           *date,
                   gint32           num_args,
                   PdbProcArg      *args,
                   gint32           num_values,
                   PdbProcArg      *values,
                   Wire            *wire,
                   WireStdProtocol *protocol)
{
  PdbProc     *pp;
  PdbWireProc *pwp;
  
  g_return_val_if_fail (wire != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), NULL);
  
  g_return_val_if_fail (protocol != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_STD_PROTOCOL (wire), NULL);
  

  pwp = (PdbWireProc *) g_object_new(PDB_TYPE_WIRE_PROC, NULL);
  pp  = PDB_PROC (pwp);

  pwp->wire     = wire;
  pwp->protocol = protocol;

  pdb_proc_set_everything (pp,
                           name,
                           blurb,
                           help,
                           version,
                           author,
                           copyright,
                           date,
                           num_args,
                           args,
                           num_values,
                           values);

  return pp;
}
