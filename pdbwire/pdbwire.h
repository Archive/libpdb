/* LIBPDBWIRE - The Procedural Database IPC Library
 * Copyright (C) 1995-2002 Peter Mattis, Spencer Kimball, and Nathan Summers 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __PDBWIRE_H__
#define __PDBWIRE_H__

#include "pdb.h"
#include "wire.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */


/* Increment every time the protocol changes non-backwards compatibly
 */
#define PDBWIRE_MAJOR_VERSION 0x0000

/* Increment every time the protocol changes backwards compatibly
 */
#define PDBWIRE_MINOR_VERSION 0x0001

enum
{
  PDBWIRE_QUIT,
  PDBWIRE_CONFIG,
  PDBWIRE_TILE_REQ,
  PDBWIRE_TILE_ACK,
  PDBWIRE_TILE_DATA,
  PDBWIRE_PROC_RUN,
  PDBWIRE_PROC_RETURN,
  PDBWIRE_TEMP_PROC_RUN,
  PDBWIRE_TEMP_PROC_RETURN,
  PDBWIRE_PROC_INSTALL,
  PDBWIRE_PROC_UNINSTALL,
  PDBWIRE_EXTENSION_ACK,
  PDBWIRE_HAS_INIT
};


typedef struct _PdbWireConfig        PdbWireConfig;
typedef struct _PdbWireTileReq       PdbWireTileReq;
typedef struct _PdbWireTileAck       PdbWireTileAck;
typedef struct _PdbWireTileData      PdbWireTileData;
typedef struct _PdbWireParam         PdbWireParam;
typedef struct _PdbWireParamDef      PdbWireParamDef;
typedef struct _PdbWireProcRun       PdbWireProcRun;
typedef struct _PdbWireProcReturn    PdbWireProcReturn;
typedef struct _PdbWireProcInstall   PdbWireProcInstall;
typedef struct _PdbWireProcUninstall PdbWireProcUninstall;


#ifdef GIMP_CRUFT
struct _PdbWireConfig
{
  guint32 version;
  guint32 tile_width;
  guint32 tile_height;
  gint32  shm_ID;
  gdouble gamma;
  gint8   install_cmap;
  gint8   unused;       /* used to be use_xshm */
  gint32  min_colors;
  gint32  gdisp_ID;
};

struct _PdbWireTileReq
{
  gint32  drawable_ID;
  guint32 tile_num;
  guint32 shadow;
};

struct _PdbWireTileData
{
  gint32   drawable_ID;
  guint32  tile_num;
  guint32  shadow;
  guint32  bpp;
  guint32  width;
  guint32  height;
  guint32  use_shm;
  guchar  *data;
};

#endif

#ifdef GIMP_CRUFT
struct _PdbWireParam
{
  guint32 type;

  union
  {
    gint32    d_int32;
    gint16    d_int16;
    gint8     d_int8;
    gdouble   d_float;
    gchar    *d_string;
    gint32   *d_int32array;
    gint16   *d_int16array;
    gint8    *d_int8array;
    gdouble  *d_floatarray;
    gchar   **d_stringarray;
    GimpRGB   d_color;
    struct
    {
      gint32 x;
      gint32 y;
      gint32 width;
      gint32 height;
    } d_region;
    gint32 d_display;
    gint32 d_image;
    gint32 d_layer;
    gint32 d_channel;
    gint32 d_drawable;
    gint32 d_selection;
    gint32 d_boundary;
    gint32 d_path;
    struct
    {
      gchar    *name;
      guint32   flags;
      guint32   size;
      gpointer  data;
    } d_parasite;
    gint32 d_status;
  } data;
};
#else
struct _PdbWireParam
{
  guint32 type;

  union
  {
    gint32    d_int32;
    gint16    d_int16;
    gint8     d_int8;
    gdouble   d_float;
    gchar    *d_string;
    gint32   *d_int32array;
    gint16   *d_int16array;
    gint8    *d_int8array;
    gdouble  *d_floatarray;
    gchar   **d_stringarray;
    gint32 d_status;
  } data;
};

#endif

struct _PdbWireParamDef
{
  guint32  type;
  gchar   *name;
  gchar   *description;
};

struct _PdbWireProcRun
{
  gchar        *name;
  guint32       nparams;
  PdbWireParam *params;
};

struct _PdbWireProcReturn
{
  gchar        *name;
  guint32       nparams;
  PdbWireParam *params;
};

struct _PdbWireProcInstall
{
  gchar           *name;
  gchar           *blurb;
  gchar           *help;
  gchar           *author;
  gchar           *copyright;
  gchar           *date;
  gchar           *menu_path;
  gchar           *image_types;
  guint32          type;
  guint32          nparams;
  guint32          nreturn_vals;
  PdbWireParamDef *params;
  PdbWireParamDef *return_vals;
};

struct _PdbWireProcUninstall
{
  gchar *name;
};


void      pdbwire_register               (WireStdProtocol       *wsp);

gboolean  pdbwire_quit_write             (WireStdProtocol       *wsp,
                                          Wire                  *wire,
                                          GError               **error);

#ifdef GIMP_CRUFT
gboolean  pdbwire_config_write           (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireConfig         *config,
                                          GError               **error);
gboolean  pdbwire_tile_req_write         (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireTileReq        *tile_req,
                                          GError               **error);
gboolean  pdbwire_tile_ack_write         (WireStdProtocol       *wsp,
                                          Wire                  *wire,
                                          GError               **error);
gboolean  pdbwire_tile_data_write        (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireTileData       *tile_data,
                                          GError               **error);
#endif

gboolean  pdbwire_proc_run_write         (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcRun        *proc_run,
                                          GError               **error);
gboolean  pdbwire_proc_return_write      (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcReturn     *proc_return,
                                          GError               **error);
gboolean  pdbwire_temp_proc_run_write    (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcRun        *proc_run,
                                          GError               **error);
gboolean  pdbwire_temp_proc_return_write (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcReturn     *proc_return,
                                          GError               **error);
gboolean  pdbwire_proc_install_write     (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcInstall    *proc_install,
                                          GError               **error);
gboolean  pdbwire_proc_uninstall_write   (WireStdProtocol       *wsp,
                                          Wire                  *wire,
				          PdbWireProcUninstall  *proc_uninstall,
                                          GError               **error);
gboolean  pdbwire_extension_ack_write    (WireStdProtocol       *wsp,
                                          Wire                  *wire,
                                          GError               **error);
gboolean  pdbwire_has_init_write         (WireStdProtocol       *wsp,
                                          Wire                  *wire,
                                          GError               **error);


G_END_DECLS

#endif /* __PDBWIRE_H__ */
