/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_STREAM_BUFFER_H__
#define __WIRE_STREAM_BUFFER_H__

#include "wirebuffer.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_STREAM_BUFFER            (wire_stream_buffer_get_type ())
#define WIRE_STREAM_BUFFER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_STREAM_BUFFER, WireStreamBuffer))
#define WIRE_STREAM_BUFFER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_STREAM_BUFFER, WireStreamBufferClass))
#define WIRE_IS_STREAM_BUFFER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_STREAM_BUFFER))
#define WIRE_IS_STREAM_BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_STREAM_BUFFER))
#define WIRE_GET_STREAM_BUFFER_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_STREAM_BUFFER, WireStreamBufferClass))

struct _WireStreamBuffer {
  WireBuffer  parent_instance;

  GByteArray *array;
  guint8     *p;
};

struct _WireStreamBufferClass 
{
  WireBufferClass  parent_class;
};

typedef struct _WireStreamBufferClass WireStreamBufferClass;


GType        wire_stream_buffer_get_type  (void) G_GNUC_CONST;

WireBuffer * wire_stream_buffer_new             (void);
WireBuffer * wire_stream_buffer_new_with_size   (gsize   size);
WireBuffer * wire_stream_buffer_new_from_buffer (guint8 *buf,
                                                 gsize   size);

gint8    wire_stream_buffer_read_int8    (WireStreamBuffer *buffer,
                                          GError          **error);
guint8   wire_stream_buffer_read_uint8   (WireStreamBuffer *buffer,
                                          GError          **error);
gint16   wire_stream_buffer_read_int16   (WireStreamBuffer *buffer,
                                          GError          **error);
guint16  wire_stream_buffer_read_uint16  (WireStreamBuffer *buffer,
                                          GError          **error);
gint32   wire_stream_buffer_read_int32   (WireStreamBuffer *buffer,
                                          GError          **error);
guint32  wire_stream_buffer_read_uint32  (WireStreamBuffer *buffer,
                                          GError          **error);
gfloat   wire_stream_buffer_read_float   (WireStreamBuffer *buffer,
                                          GError          **error);
gdouble  wire_stream_buffer_read_double  (WireStreamBuffer *buffer,
                                          GError          **error);
gchar *  wire_stream_buffer_read_string  (WireStreamBuffer *buffer,
                                          GError          **error);

gboolean  wire_stream_buffer_read_int8_v   (WireStreamBuffer *buffer,
                                            gint8            *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_uint8_v  (WireStreamBuffer *buffer,
                                            guint8           *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_int16_v  (WireStreamBuffer *buffer,
                                            gint16           *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_uint16_v (WireStreamBuffer *buffer,
                                            guint16          *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_int32_v  (WireStreamBuffer *buffer,
                                            gint32           *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_uint32_v (WireStreamBuffer *buffer,
                                            guint32          *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_float_v  (WireStreamBuffer *buffer,
                                            gfloat           *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_double_v (WireStreamBuffer *buffer,
                                            gdouble          *data,
                                            gsize             count,
                                            GError          **error);
gboolean  wire_stream_buffer_read_string_v (WireStreamBuffer *buffer,
                                            gchar           **data,
                                            gsize             count,
                                            GError          **error);

void      wire_stream_buffer_write_int8   (WireStreamBuffer *buffer,
                                           gint8             data);
void      wire_stream_buffer_write_uint8  (WireStreamBuffer *buffer,
                                           guint8            data);
void      wire_stream_buffer_write_int16  (WireStreamBuffer *buffer,
                                           gint16            data);
void      wire_stream_buffer_write_uint16 (WireStreamBuffer *buffer,
                                           guint16           data);
void      wire_stream_buffer_write_int32  (WireStreamBuffer *buffer,
                                           gint32            data);
void      wire_stream_buffer_write_uint32 (WireStreamBuffer *buffer,
                                           guint32           data);
void      wire_stream_buffer_write_float  (WireStreamBuffer *buffer,
                                           gfloat            data);
void      wire_stream_buffer_write_double (WireStreamBuffer *buffer,
                                           gdouble           data);
void      wire_stream_buffer_write_string (WireStreamBuffer *buffer,
                                           gchar            *data);

void      wire_stream_buffer_write_int8_v   (WireStreamBuffer *buffer,
                                             gint8            *data,
                                             gsize             count);
void      wire_stream_buffer_write_uint8_v  (WireStreamBuffer *buffer,
                                             guint8           *data,
                                             gsize             count);
void      wire_stream_buffer_write_int16_v  (WireStreamBuffer *buffer,
                                             gint16           *data,
                                             gsize             count);
void      wire_stream_buffer_write_uint16_v (WireStreamBuffer *buffer,
                                             guint16          *data,
                                             gsize             count);
void      wire_stream_buffer_write_int32_v  (WireStreamBuffer *buffer,
                                             gint32          *data,
                                             gsize             count);
void      wire_stream_buffer_write_uint32_v (WireStreamBuffer *buffer,
                                             guint32          *data,
                                             gsize             count);
void      wire_stream_buffer_write_float_v  (WireStreamBuffer *buffer,
                                             gfloat           *data,
                                             gsize             count);
void      wire_stream_buffer_write_double_v (WireStreamBuffer *buffer,
                                             gdouble          *data,
                                             gsize             count);
void      wire_stream_buffer_write_string_v (WireStreamBuffer *buffer,
                                             gchar           **data,
                                             gsize             count);

G_END_DECLS

#endif /* __WIRE_STREAM_BUFFER_H__ */
