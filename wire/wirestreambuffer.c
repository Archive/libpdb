/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <glib.h>

#include "wiretypes.h"

#include "wirestreambuffer.h"


/**
 * wire_stream_buffer_read_int32:
 * @buffer: a #WireStreamBuffer.
 * @error: a pointer to a location to store a #GError, or NULL
 *
 * Reads a 32-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gint32
wire_stream_buffer_read_int32 (WireStreamBuffer    *buffer,
                               GError             **error)
{
  gint32 data;

  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) &data, 4, error))
    return -1;

  /* wow, lots of byte swapping */

  data = g_ntohl (data);

  return data;
}

/**
 * wire_stream_buffer_read_int32_v:
 * @buffer: a #WireStreamBuffer.
 * @error: a pointer to a location to store a #GError, or NULL
 *
 * Reads a series of 32-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_int32_v (WireStreamBuffer    *buffer,
                                 gint32              *data,
                                 gsize                count,
                                 GError             **error)
{
  if (count > 0)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) data, count * 4, error))
        return FALSE;

      /* wow, lots of byte swapping */

      while (count--)
        {
          *data = g_ntohl (*data);
          data++;
        }
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_int16:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a 16-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gint16
wire_stream_buffer_read_int16 (WireStreamBuffer     *buffer,
                               GError             **error)
{
  gint16 data;
  
  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) &data, 2, error))
    return -1;

  data = g_ntohs (data);

  return data;
}

/**
 * wire_stream_buffer_read_int16_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of 16-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_int16_v (WireStreamBuffer     *buffer,
                                 gint16               *data,
                                 gsize                 count,
                                 GError              **error)

{
  if (count > 0)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) data, count * 2, error))
        return FALSE;

      while (count--)
        {
          *data = g_ntohs (*data);
          data++;
        }
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_int8:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a 8-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gint8
wire_stream_buffer_read_int8 (WireStreamBuffer     *buffer,
                              GError              **error)

{
  gint8 data;

  //if (!wire_buffercol_read (WIRE_PROTOCOL (buffer), &data, 1))
    return -1;

  return data;
}

/**
 * wire_stream_buffer_read_int8_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of 8-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_int8_v (WireStreamBuffer     *buffer,
                                gint8                *data,
                                gsize                 count,
                               GError               **error)

{
  //return wire_buffercol_read (WIRE_PROTOCOL (buffer), data, count);
  return -1;
}
/**
 * wire_stream_buffer_read_uint32:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a unsigned 32-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
guint32
wire_stream_buffer_read_uint32 (WireStreamBuffer    *buffer,
                                GError             **error)

{
  guint32 data;

  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) &data, 4, error))
    return -1;

  /* wow, lots of byte swapping */

  data = g_ntohl (data);

  return data;
}

/**
 * wire_stream_buffer_read_uint32_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of unsigned 32-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_uint32_v (WireStreamBuffer    *buffer,
                                  guint32             *data,
                                  gsize                count,
                                  GError             **error)
{
  if (count > 0)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) data, count * 4, error))
        return FALSE;

      /* wow, lots of byte swapping */

      while (count--)
        {
 *data = g_ntohl (*data);
 data++;
        }
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_uint16:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 *
 * Reads a unsigned 16-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
guint16
wire_stream_buffer_read_uint16 (WireStreamBuffer     *buffer,
                                GError              **error)

{
  guint16 data;
  
  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) &data, 2, error))
    return -1;

  data = g_ntohs (data);

  return data;
}

/**
 * wire_stream_buffer_read_uint16_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of unsigned 16-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_uint16_v (WireStreamBuffer     *buffer,
                                  guint16              *data,
                                  gsize                 count,
                                  GError              **error)

{
  if (count > 0)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) data, count * 2, error))
        return FALSE;

      while (count--)
        {
          *data = g_ntohs (*data);
          data++;
        }
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_uint8:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a 8-bit integer from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
guint8
wire_stream_buffer_read_uint8 (WireStreamBuffer     *buffer,
                               GError              **error)


{
  guint8 data;

  //if (!wire_buffercol_read (WIRE_PROTOCOL (buffer), &data, 1))
    return -1;

  return data;
}

/**
 * wire_stream_buffer_read_uint8_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of 8-bit integers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_uint8_v (WireStreamBuffer    *buffer,
                                 guint8              *data,
                                 gsize                count,
                                 GError             **error)
{
  //return wire_buffercol_read (WIRE_PROTOCOL (buffer), data, count);
  return FALSE;
}

/**
 * wire_stream_buffer_read_float:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a 32-bit floating-point number from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gfloat
wire_stream_buffer_read_float  (WireStreamBuffer    *buffer,
                                GError             **error)
{
  gfloat  *t;
  guint32  tmp;

  t = (gfloat *) &tmp;

  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) tmp, 4, error))
    return -1;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  tmp = g_ntohl (tmp);
#endif

  return *t;
}

/**
 * wire_stream_buffer_read_float_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of 32-bit floating-point numbers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_float_v (WireStreamBuffer    *buffer,
                                 gfloat              *data,
                                 gsize                count,
                                 GError             **error)
{
  gfloat  *t;
  guint32  tmp;
  gint     i;

  t = (gfloat *) &tmp;

  for (i = 0; i < count; i++)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) tmp, 4, error))
        return FALSE;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
      tmp = g_ntohl (tmp);
#endif

      data[i] = *t;
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_double:
 * @buffer: a #WireStreamBuffer.
 * @Wire: the #Wire to read from.

 *
 * Reads a 64-bit floating-point number from @wire using @buffer.
 *
 * Returns: the value read, or -1 if unsuccessful. Note that since
 * -1 may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gdouble
wire_stream_buffer_read_double (WireStreamBuffer    *buffer,
                                GError             **error)
{
  gdouble *t;
  guint32  tmp[2];
#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  guint32  swap;
#endif

  t = (gdouble *) tmp;

  if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) tmp, 8, error))
    return FALSE;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  swap   = g_ntohl (tmp[1]);
  tmp[1] = g_ntohl (tmp[0]);
  tmp[0] = swap;
#endif

  return *t;
}

/**
 * wire_stream_buffer_read_double_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of 64-bit floating-point numbers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_double_v (WireStreamBuffer    *buffer,
                                  gdouble             *data,
                                  gsize                count,
                                GError               **error)

{
  gdouble *t;
  guint32  tmp[2];
  gint     i;
#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  guint32  swap;
#endif

  t = (gdouble *) tmp;

  for (i = 0; i < count; i++)
    {
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) tmp, 8, error))
        return FALSE;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
      swap   = g_ntohl (tmp[1]);
      tmp[1] = g_ntohl (tmp[0]);
      tmp[0] = swap;
#endif

      data[i] = *t;
    }

  return TRUE;
}

/**
 * wire_stream_buffer_read_string:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.

 *
 * Reads a string from @wire using @buffer.
 *
 * Returns: the value read, or NULL if unsuccessful. Note that since
 * NULL may be a valid value, the only sure way to check for success is to
 * check the value of @error.
 */
gchar *
wire_stream_buffer_read_string (WireStreamBuffer     *buffer,
                                GError              **error)


{
  guint32 len;
  gchar *data;

  if (!wire_stream_buffer_read_int32_v (buffer, &len, 1, error))
    return NULL;

  if (len > 0)
    {
      data = g_new (gchar, len);
      if (! wire_stream_buffer_read_int8_v (buffer, (guint8 *) data, len, error))
        {
 g_free (data);
 return NULL;
        }
    }
  else
    {
      return NULL;
    }

  return data;
}

/**
 * wire_stream_buffer_read_string_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to read from.
 * @data: a location to store the data in.
 * @count: the number of elements to read.

 *
 * Reads a series of strings numbers from @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
gboolean
wire_stream_buffer_read_string_v (WireStreamBuffer     *buffer,
                                  gchar               **data,
                                  gsize                 count,
                                  GError              **error)
{
  GError *tmp_error;
  gint    i;

  for (i = 0; i < count; i++)
    {
      data[i] = wire_stream_buffer_read_string (buffer, &tmp_error);

      if (tmp_error)
        {
 g_propagate_error (error, tmp_error);

 return FALSE;
        }
    }

  return TRUE;
}


/**
 * wire_stream_buffer_write_int32:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.

 *
 * Writes a 32-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int32 (WireStreamBuffer     *buffer,
                                gint32            data)

{
  guint32 tmp;

  tmp = g_htonl (data);
  wire_stream_buffer_write_int8_v (buffer, (guint8 *) &tmp, 4);
}

/**
 * wire_stream_buffer_write_int32_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.

 *
 * Writes a series of 32-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int32_v (WireStreamBuffer     *buffer,
                        gint32           *data,
                        gsize            count)

{
  gsize i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
          wire_stream_buffer_write_int32 (buffer, data[i]);
        }
    }
}

/**
 * wire_stream_buffer_write_int16:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 *
 * Writes a 16-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int16 (WireStreamBuffer    *buffer,
                                gint16               data)

{
  guint16 tmp;

  tmp = g_htons (data);
  wire_stream_buffer_write_int8_v (buffer, (guint8 *) &tmp, 2);
}

/**
 * wire_stream_buffer_write_int16_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.
 *
 * Writes a series of 16-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int16_v (WireStreamBuffer    *buffer,
                                  gint16              *data,
                                  gsize                count)

{
  gint    i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
          wire_stream_buffer_write_int16 (buffer, data[i]);
        }
    }
}

/**
 * wire_stream_buffer_write_int8:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 *
 * Writes a 8-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int8 (WireStreamBuffer     *buffer,
                               gint8                 data)

{
  //return wire_buffercol_write (WIRE_PROTOCOL (buffer), &data, 1);
}

/**
 * wire_stream_buffer_write_int8_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.
 *
 * Writes a series of 8-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_int8_v (WireStreamBuffer     *buffer,
                                 gint8                *data,
                                 gsize                 count)

{
  //return wire_buffercol_write (WIRE_PROTOCOL (buffer), data, count);
}

/**
 * wire_stream_buffer_write_uint32:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.

 *
 * Writes an unsigned 32-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint32 (WireStreamBuffer     *buffer,
                                guint32                data)

{
  guint32 tmp;

  tmp = g_htonl (data);
  wire_stream_buffer_write_int8_v (buffer, (guint8 *) &tmp, 4);
}

/**
 * wire_stream_buffer_write_uint32_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.
 *
 * Writes a series of unsigned 32-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint32_v (WireStreamBuffer     *buffer,
                                   guint32              *data,
                                   gsize                 count)

{
  gsize i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
           wire_stream_buffer_write_int32 (buffer, data[i]);
        }
    }
}

/**
 * wire_stream_buffer_write_uint16:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 *
 * Writes an unsigned 16-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint16 (WireStreamBuffer    *buffer,
                                 guint16              data)

{
  guint16 tmp;

  tmp = g_htons (data);
  wire_stream_buffer_write_int8_v (buffer, (guint8 *) &tmp, 2);
}

/**
 * wire_stream_buffer_write_uint16_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.
 *
 * Writes a series of unsigned 16-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint16_v (WireStreamBuffer    *buffer,
                                   guint16             *data,
                                   gsize                count)

{
  gint    i;

  if (count > 0)
    {
      for (i = 0; i < count; i++)
        {
          wire_stream_buffer_write_int16 (buffer, data[i]);
        }
    }
}

/**
 * wire_stream_buffer_write_uint8:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 *
 * Writes an unsigned 8-bit integer on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint8 (WireStreamBuffer     *buffer,
                                guint8                data)
{
  //return wire_buffercol_write (WIRE_PROTOCOL (buffer), &data, 1);
}

/**
 * wire_stream_buffer_write_uint8_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.
 *
 * Writes a series of unsigned 8-bit integers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_uint8_v (WireStreamBuffer     *buffer,

                        guint8           *data,
                        gsize             count)

{
  //return wire_buffercol_write (WIRE_PROTOCOL (buffer), data, count);
}

/**
 * wire_stream_buffer_write_float:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.

 *
 * Writes a 32-bit floating-point number on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_float  (WireStreamBuffer    *buffer,

                       gfloat           data)

{
  gfloat  *t;
  guint32  tmp;

  t = (gfloat *) &tmp;

  *t = data;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  tmp = g_htonl (tmp);
#endif

  wire_stream_buffer_write_int8_v (buffer, (guint8 *) tmp, 4);

#if 0
  {
    gint j;

    g_print ("Wire representation of %f:\t", data[i]);

    for (j = 0; j < 4; j++)
      {
        g_print ("%02x ", ((guchar *) tmp)[j]);
      }

    g_print ("\n");
  }
#endif
}

/**
 * wire_stream_buffer_write_float_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.

 *
 * Writes a series of 32-bit floating-point numbers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_float_v (WireStreamBuffer    *buffer,

                         gfloat          *data,
                         gsize            count)

{
  gint     i;

  for (i = 0; i < count; i++)
    wire_stream_buffer_write_float (buffer, data[i]);
}

/**
 * wire_stream_buffer_write_double:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.

 *
 * Writes a 64-bit floating-point number on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_double (WireStreamBuffer    *buffer,

                       gdouble          data)

{
  gdouble *t;
  guint32  tmp[2];
#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  guint32  swap;
#endif

  t = (gdouble *) tmp;

  *t = data;

#if (G_BYTE_ORDER == G_LITTLE_ENDIAN)
  swap   = g_htonl (tmp[1]);
  tmp[1] = g_htonl (tmp[0]);
  tmp[0] = swap;
#endif

  wire_stream_buffer_write_int8_v (buffer, (guint8 *) tmp, 8);

#if 0
  {
    gint j;

    g_print ("Wire representation of %f:\t", data[i]);

    for (j = 0; j < 8; j++)
      {
        g_print ("%02x ", ((guchar *) tmp)[j]);
      }

    g_print ("\n");
  }
#endif
}

/**
 * wire_stream_buffer_write_double_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.

 *
 * Writes a series of 64-bit floating-point numbers on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_double_v (WireStreamBuffer    *buffer,

                         gdouble         *data,
                         gsize            count)

{
  gint     i;

  for (i = 0; i < count; i++)
    wire_stream_buffer_write_double (buffer, data[i]);
}

/**
 * wire_stream_buffer_write_string:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 *
 * Writes a string on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_string (WireStreamBuffer     *buffer,
                                 gchar                *data)

{
  guint32 len;

  if (data)
    len = strlen (data) + 1;
  else
    len = 0;

  wire_stream_buffer_write_int32 (buffer, len);
  
  if (len > 0)
    wire_stream_buffer_write_int8_v (buffer, (guint8 *) data, len);
}

/**
 * wire_stream_buffer_write_string_v:
 * @buffer: a #WireStreamBuffer.
 * @wire: the #Wire to write to.
 * @data: the data to write.
 * @count: the number of elements to write.

 *
 * Writes a series of strings on @wire using @buffer.
 *
 * Returns: TRUE on success. 
 */
void
wire_stream_buffer_write_string_v (WireStreamBuffer     *buffer,
                                   gchar               **data,
                                   gsize                 count)

{
  gint    i;

  for (i = 0; i < count; i++)
    wire_stream_buffer_write_string (buffer, data[i]);
}
