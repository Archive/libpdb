/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_FIXED_BUFFER_H__
#define __WIRE_FIXED_BUFFER_H__

#include "wirebuffer.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_FIXED_BUFFER            (wire_fixed_buffer_get_type ())
#define WIRE_FIXED_BUFFER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_FIXED_BUFFER, WireFixedBuffer))
#define WIRE_FIXED_BUFFER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_FIXED_BUFFER, WireFixedBufferClass))
#define WIRE_IS_FIXED_BUFFER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_FIXED_BUFFER))
#define WIRE_IS_FIXED_BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_FIXED_BUFFER))
#define WIRE_GET_FIXED_BUFFER_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_FIXED_BUFFER, WireFixedBufferClass))

struct _WireFixedBuffer {
  WireBuffer  parent_instance;

  guint8     *data;
  gsize       size;
  gboolean    free_on_finalize;
  GObject    *unref_on_finalize;
};

struct _WireFixedBufferClass 
{
  WireBufferClass  parent_class;
};

typedef struct _WireFixedBufferClass WireFixedBufferClass;


GType        wire_fixed_buffer_get_type  (void) G_GNUC_CONST;

WireBuffer * wire_fixed_buffer_new              (guint8 *buf,
                                                 gsize   size);
WireBuffer * wire_fixed_buffer_new_copy         (guint8 *buf,
                                                 gsize   size);
WireBuffer * wire_fixed_buffer_new_from_object  (guint8  *buf,
                                                 gsize    size,
                                                 GObject *object);
G_END_DECLS

#endif /* __WIRE_FIXED_BUFFER_H__ */
