/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <glib.h>

#ifdef G_OS_WIN32
#include <process.h>
#include <io.h>
#endif

#include "wiretypes.h"

#include "wiregiochannel.h"

#include "wirefixedbuffer.h"

#include "wireerror.h"

#include "wireintl.h"



static void         wire_g_io_channel_class_init (WireClass   *klass); 
static void         wire_g_io_channel_finalize   (GObject     *object);

static gboolean     wire_g_io_channel_send       (Wire        *wire,
			                          WireBuffer  *buf,
			                          GError     **error);
static WireBuffer * wire_g_io_channel_receive    (Wire        *wire,
		                                  GError     **error);
static gboolean     wire_g_io_channel_flush      (Wire        *wire,
                                                  GError     **error);

static gboolean     wire_g_io_channel_write      (WireGIOChannel  *wire,
			                          guint8          *buf,
			                          guint32          size,
			                          GError         **error);
static gboolean     wire_g_io_channel_read       (WireGIOChannel  *wire,
			                          guint8          *buf,
			                          guint32          size,
                                                  GError         **error);

static gboolean     wire_g_io_channel_write_buf  (WireFixedBuffer  *buf,
                                                  gpointer          data,
                                                  GError          **error);

static WireClass *parent_class = NULL; 

GType
wire_g_io_channel_get_type (void)
{
  static GType wire_type = 0;

  if (! wire_type)
    {
      static const GTypeInfo wire_info =
      {
        sizeof (WireGIOChannelClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) wire_g_io_channel_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (Wire),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) NULL,
      };

      wire_type = g_type_register_static (WIRE_TYPE_WIRE,
	 				  "WireGIOChannel", 
                                          &wire_info, 0);
    }

  return wire_type;
}


static void
wire_g_io_channel_class_init (WireClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = wire_g_io_channel_finalize;

  klass->send    = wire_g_io_channel_send;
  klass->receive = wire_g_io_channel_receive;
  klass->flush   = wire_g_io_channel_flush;

}

static void
wire_g_io_channel_finalize (GObject *object)
{
  WireGIOChannel *wire;

  g_return_if_fail (WIRE_IS_G_IO_CHANNEL (object));
  
  wire = WIRE_G_IO_CHANNEL (object);
  
  if (wire->read)
    g_io_channel_unref(wire->read);

  if (wire->write)
    g_io_channel_unref(wire->write);



  G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * wire_g_io_channel_new:
 * @read: the #GIOChannel to read from
 * @write: the #GIOChannel to write to. Can be the same as @read.
 *
 * Creates a #Wire using the #GIOChannel backend. The #GIOChannels used are 
 * required to be using the NULL encoding (that is, straight binary 
 * passthrough.) wire_g_io_channel_new() will attempt to set the #GIOChannels
 * it recieves as arguments to use the NULL encoding; however, in certain cases
 * (which should not happen if this library is used normally) this may 
 * silently fail. See the glib documentation for details.
 *
 * Returns: a #WireGIOChannel
 */
 

Wire *
wire_g_io_channel_new (GIOChannel *read,
                       GIOChannel *write)
{
  Wire *wire;
  WireGIOChannel *wire_gio;

  g_return_val_if_fail (read != NULL, NULL);
  g_return_val_if_fail (write != NULL, NULL);
  g_return_val_if_fail (g_io_channel_get_flags (read) & G_IO_FLAG_IS_READABLE, NULL);
  g_return_val_if_fail (g_io_channel_get_flags (write) & G_IO_FLAG_IS_WRITEABLE, NULL);
  
  wire     = (Wire *) g_object_new(WIRE_TYPE_G_IO_CHANNEL, NULL);
  wire_gio = (WireGIOChannel *) wire;

  wire_gio->read = read;
  g_io_channel_ref (read);
  g_io_channel_set_encoding (read, NULL, NULL);
  
  wire_gio->write = write;
  g_io_channel_ref (write);
  g_io_channel_set_encoding (write, NULL, NULL);

  return wire;
}


static gboolean 
wire_g_io_channel_read (WireGIOChannel  *wire,
		        guint8          *buf,
		        guint32          count,
	                GError         **error)
{
  GIOStatus       status;
  gsize           bytes;

  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (wire), FALSE);

  while (count > 0)
    {
      do
        {
          bytes = 0;
          status = g_io_channel_read_chars (wire->read,
                                            (gchar *) buf, count,
                                            &bytes,
                                            error);
        }
      while (status == G_IO_STATUS_AGAIN);

      if (status != G_IO_STATUS_NORMAL)
        {
          ((Wire *) wire)->error_val = TRUE;
          return FALSE;
        }

      if (bytes == 0)
        {
          /* G_IO_STATUS_EOF? */
          g_set_error (error, WIRE_ERROR, WIRE_ERROR_UNEXPECTED_DISCONNECT,
            _("wire_g_io_channel_read(): unexpected EOF"));
            
          ((Wire *) wire)->error_val = TRUE;
          return FALSE;
        }

      count -= bytes;
      buf   += bytes;
    }

  return TRUE;
}

static WireBuffer * 
wire_g_io_channel_receive (Wire        *wire,
	                   GError     **error)
{
  WireGIOChannel  *wire_gio;
  guint32          count, lecount;
  WireBuffer      *fixedbuf;
  guint8          *buf;

  
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (wire), FALSE);

  wire_gio = (WireGIOChannel *) wire;

  if (! wire_g_io_channel_read (wire_gio, (guint8 *) &lecount, 4, error))
    return NULL;
    
  count = GUINT32_FROM_LE (lecount);



  if (count > 0)
    {
      buf = g_new (guint8, count);
      
      if (! wire_g_io_channel_read (wire_gio, buf, count, error))
        {
          g_free (buf);

          return NULL;
        }
    }
  else
    buf = NULL;
  
  fixedbuf = wire_fixed_buffer_new (buf, count);
  
  return fixedbuf;
}

static gboolean
wire_g_io_channel_write  (WireGIOChannel   *wire,
	                  guint8           *buf,
	                  guint32           count,
	                  GError          **error)
{
  GIOStatus        status;
  gsize            bytes;

  
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (wire), FALSE);

  while (count > 0)
    {
      do
        {
          bytes = 0;
          status = g_io_channel_write_chars (wire->write,
                                             (gchar *) buf, count,
                                             &bytes,
                                             error);
        }
      while (status == G_IO_STATUS_AGAIN);
 /* FIXME EOF? */

      if (status != G_IO_STATUS_NORMAL)
        {
          ((Wire *) wire)->error_val = TRUE;
          return FALSE;
        }

      count -= bytes;
      buf   += bytes;
    }

  return TRUE;
}

static gboolean
wire_g_io_channel_write_buf  (WireFixedBuffer  *buf,
	                      gpointer          data,
	                      GError          **error)
{
  WireGIOChannel  *wire_gio;
  
  g_return_val_if_fail (data != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (data), FALSE);

  wire_gio = (WireGIOChannel *) data;

  if (! wire_g_io_channel_write (wire_gio, buf->data, buf->size, error))
    return FALSE;
  
  return TRUE;
}


static gboolean
wire_g_io_channel_send  (Wire        *wire,
	                 WireBuffer  *buf,
	                 GError     **error)
{
  WireGIOChannel  *wire_gio;
  guint32          count, lecount;
  
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (wire), FALSE);

  wire_gio = (WireGIOChannel *) wire;

  count   = wire_buffer_get_total_size (buf);
  lecount = GUINT32_TO_LE (count);

  if (! wire_g_io_channel_write (wire_gio, (guint8 *) &lecount, 4, error))
    return FALSE;

  if (count > 0)
    {
      if (! wire_buffer_foreach (buf, wire_g_io_channel_write_buf, wire_gio, error))
        {

          return FALSE;
        }


    }
  
  return TRUE;
}

static gboolean
wire_g_io_channel_flush (Wire       *wire,
                         GError    **error)
{
  WireGIOChannel *wire_gio;
  GIOStatus       status;

  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_G_IO_CHANNEL (wire), FALSE);

  wire_gio = (WireGIOChannel *) wire;

  do
    {
      status = g_io_channel_flush (wire_gio->write,
                                   error);
    }
  while (status == G_IO_STATUS_AGAIN);

/* FIXME: EOF? */
 
  if (status != G_IO_STATUS_NORMAL)
    {
      wire->error_val = TRUE;
      return FALSE;
    }

  return TRUE;
}

