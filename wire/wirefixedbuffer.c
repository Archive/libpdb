/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include "wiretypes.h"

#include "wirefixedbuffer.h"


static void     wire_fixed_buffer_class_init   (WireBufferClass   *klass); 
static void     wire_fixed_buffer_finalize     (GObject           *object);

static WireFixedBuffer * wire_fixed_buffer_read         (WireBuffer        *buf,
                                                         GError           **error);
static gsize             wire_fixed_buffer_get_size     (WireBuffer        *object);



static GObjectClass  *parent_class;


GType
wire_fixed_buffer_get_type (void)
{
  static GType buffer_type = 0;

  if (! buffer_type)
    {
      static const GTypeInfo buffer_info =
      {
        sizeof (WireFixedBufferClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) wire_fixed_buffer_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data     */
        sizeof (WireFixedBuffer),
        0,              /* n_preallocs    */
        (GInstanceInitFunc) NULL,
      };

      buffer_type = g_type_register_static (WIRE_TYPE_BUFFER,
                                            "WireFixedBuffer", 
                                            &buffer_info, 0);
    }

  return buffer_type;
}

static void
wire_fixed_buffer_class_init (WireBufferClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = wire_fixed_buffer_finalize; 

  klass->get_size = wire_fixed_buffer_get_size;
  klass->read     = wire_fixed_buffer_read;
}

static WireFixedBuffer * 
wire_fixed_buffer_read (WireBuffer  *object,
                        GError     **error)
{
  WireFixedBuffer *buffer;

  g_return_val_if_fail (WIRE_IS_FIXED_BUFFER (object), NULL);
  
  buffer = WIRE_FIXED_BUFFER (object);

  g_object_ref (object);
  
  return buffer; 
}

WireBuffer *
wire_fixed_buffer_new   (guint8      *buf,
                         gsize        size)
{
  WireFixedBuffer *buffer;

  buffer = (WireFixedBuffer *) g_object_new (WIRE_TYPE_FIXED_BUFFER, NULL);
  
  if (size > 1 && buf)
    {
      buffer->data = buf;
      buffer->size = size;
      
      buffer->free_on_finalize = TRUE;
    }
  else
    {
      buffer->data = NULL;
      buffer->size = 0;
      
      buffer->free_on_finalize = FALSE;
    }

  buffer->unref_on_finalize = NULL;
    
  return (WireBuffer *) buffer; 
}

WireBuffer *
wire_fixed_buffer_new_copy (guint8      *buf,
                            gsize        size)
{
  WireFixedBuffer *buffer;

  buffer = (WireFixedBuffer *) g_object_new (WIRE_TYPE_FIXED_BUFFER, NULL);
  
  if (size > 1 && buf)
    {
      buffer->data = g_memdup (buf, size);
      buffer->size = size;

      buffer->free_on_finalize = TRUE;
    }
  else
    {
      buffer->data = NULL;
      buffer->size = 0;

      buffer->free_on_finalize = FALSE;
    }

  buffer->unref_on_finalize = NULL;

  return (WireBuffer *) buffer; 
}

WireBuffer *
wire_fixed_buffer_new_from_object (guint8      *buf,
                                   gsize        size,
                                   GObject     *object)
{
  WireFixedBuffer *buffer;

  buffer = (WireFixedBuffer *) g_object_new (WIRE_TYPE_FIXED_BUFFER, NULL);
  
  if (buffer->data) 
    g_free (buffer->data);


  if (size > 1 && buf)
    {
      buffer->data = g_memdup (buf, size);
      buffer->size = size;
    }
  else
    {
      buffer->data = NULL;
      buffer->size = 0;
    }

  buffer->free_on_finalize  = FALSE;
  buffer->unref_on_finalize = object; 

  return (WireBuffer *) buffer;
}

static gsize
wire_fixed_buffer_get_size (WireBuffer  *object)
{
  WireFixedBuffer *buffer;

  g_return_val_if_fail (WIRE_IS_FIXED_BUFFER (object), 0);
  
  buffer = WIRE_FIXED_BUFFER (object);
  
  return buffer->size; 
}

static void
wire_fixed_buffer_finalize (GObject *object)
{
  WireFixedBuffer *buffer;

  g_return_if_fail (WIRE_IS_FIXED_BUFFER (object));
  
  buffer = WIRE_FIXED_BUFFER (object);
  
  if (buffer->data && buffer->free_on_finalize) 
    {
      g_free (buffer->data);

      buffer->data = NULL;
    }
    
  if (buffer->unref_on_finalize)
    {
      g_object_unref (buffer->unref_on_finalize);

      buffer->unref_on_finalize = NULL;
    }

  G_OBJECT_CLASS (parent_class)->finalize (object);
}
