/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <glib.h>

#ifdef G_OS_WIN32
#include <process.h>
#include <io.h>
#endif

#include "wiretypes.h"

#include "wirewire.h"
#include "wiremsg.h"
#include "wiremsgtype.h"

#include "wirefixedbuffer.h"

#include "wirestdprotocol.h"

#include "wireintl.h"

static void      wire_std_protocol_init         (WireStdProtocol       *wsp);
static void      wire_std_protocol_class_init   (WireStdProtocolClass  *klass); 
static void      wire_std_protocol_finalize     (GObject               *object);

static gboolean  wire_std_protocol_register_msg (WireProtocol          *proto,
                                                 WireMsgType           *msg_type);

static gboolean wire_std_protocol_send          (WireProtocol          *proto,
                                                 Wire                  *wire,
                                                 WireMsg               *msg,
                                                 GError               **error);
static WireMsg * wire_std_protocol_receive      (WireProtocol          *wsp,
                                                 Wire                  *wire,
                                                 GError               **error);

static gboolean wire_std_protocol_connect       (WireProtocol          *proto,
                                                 Wire                  *wire);
static gboolean wire_std_protocol_disconnect    (WireProtocol          *proto,
                                                 Wire                  *wire);

static GObjectClass *parent_class = NULL; 

GType
wire_std_protocol_get_type (void)
{
  static GType wsp_type = 0;

  if (! wsp_type)
    {
      static const GTypeInfo wsp_info =
      {
        sizeof (WireProtocolClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) wire_std_protocol_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data     */
        sizeof (WireStdProtocol),
        0,              /* n_preallocs    */
        (GInstanceInitFunc) wire_std_protocol_init,
      };

      wsp_type = g_type_register_static (WIRE_TYPE_PROTOCOL,
                                         "WireStdProtocol", 
                                         &wsp_info, 0);
    }

  return wsp_type;
}

static void
wire_std_protocol_class_init (WireStdProtocolClass *klass)
{
  GObjectClass *object_class;
  WireProtocolClass *protocol_class;

  object_class = G_OBJECT_CLASS (klass);
  protocol_class = WIRE_PROTOCOL_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = wire_std_protocol_finalize;

  protocol_class->register_msg = wire_std_protocol_register_msg;
  
  protocol_class->send       = wire_std_protocol_send;
  protocol_class->receive    = wire_std_protocol_receive;
  protocol_class->connect    = wire_std_protocol_connect;
  protocol_class->disconnect = wire_std_protocol_disconnect;
}

static void
wire_std_protocol_init (WireStdProtocol *wsp)
{
  wsp->msg_table = wire_msg_type_table_new ();
}

static void
wire_std_protocol_finalize (GObject *object)
{
  WireStdProtocol *wsp;

  g_return_if_fail (WIRE_IS_STD_PROTOCOL (object));
  
  wsp = WIRE_STD_PROTOCOL (object);

  if (wsp->msg_table)
    {
      wire_msg_type_table_destroy (wsp->msg_table);
    
      wsp->msg_table = NULL;
    }

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * wire_std_protocol_new:
 * @server: whether the #WireStdProtocol should be a server or a client.
 *
 * Creates a new #WireStdProtocol, with the server flag set according to @server.
 *
 * Returns: a new #WireStdProtocol.
 */
WireProtocol *
wire_std_protocol_new (gboolean server) 
{
  GObject *gobj;
  WireProtocol *wp;

  gobj = g_object_new (WIRE_TYPE_STD_PROTOCOL, NULL);
  wp = WIRE_PROTOCOL (gobj);

  wp->server = server;

  return wp;
}

static gboolean
wire_std_protocol_register_msg (WireProtocol *proto,
                                WireMsgType  *msg_type)
{
  WireStdProtocol *wsp;

  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_STD_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (msg_type != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_MSG_TYPE (msg_type), FALSE);

  wsp = WIRE_STD_PROTOCOL (proto);

  wire_msg_type_table_insert (wsp->msg_table, msg_type);

  return TRUE;
}


static gboolean
wire_std_protocol_send (WireProtocol  *proto,
                        Wire          *wire,
                        WireMsg       *msg,
                        GError       **error)
{
  WireStdProtocol *wsp;
  guint32 msg_typeid;

  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_STD_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (msg != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_MSG (msg), FALSE);

  wsp = WIRE_STD_PROTOCOL (proto);

  if (wire->error_val)
    return !wire->error_val;

  msg_typeid = wire_msg_type_table_lookup_id (wsp->msg_table,
                                              G_TYPE_FROM_INSTANCE (msg));

  if (!msg_typeid)
    {
      g_error (_("wire_std_protocol_send(): could not find typeid for message"));

      return FALSE;
    }
/*
  if (! wire_msg_write_int32 (proto, wire, msg_typeid, error))
    return FALSE;
*/
  return wire_msg_write (msg, proto, wire, error);
}

static WireMsg *
wire_std_protocol_receive (WireProtocol  *proto,
                           Wire          *wire,
                           GError       **error)
{
  WireStdProtocol *wsp;
  GError *tmp_error = NULL;
  guint32 msg_typeid;
  WireMsgType *msg_type;
  WireMsg *msg;
  

  g_return_val_if_fail (wire != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), NULL);
  g_return_val_if_fail (proto != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_STD_PROTOCOL (proto), NULL);

  wsp = WIRE_STD_PROTOCOL (proto);
  
  if (wire->error_val)
    return NULL;
/*
  msg_typeid = wire_msg_read_int32 (proto, wire, &tmp_error);
  if (tmp_error)
    {
      g_propagate_error (error, tmp_error);
    
      return NULL;
    }
*/
  msg_type = wire_msg_type_table_lookup_type (wsp->msg_table, msg_typeid);
  if (!msg_type)
    {
      g_error (_("wire_std_protocol_receive(): could not find handler for message type %d"), msg_typeid);

      return NULL;
    }

  msg = wire_msg_new_from_type (msg_type);
 
  if (! wire_msg_read (msg, WIRE_PROTOCOL (wsp), wire, error))
    {
      g_object_unref (G_OBJECT (msg));

      return NULL;
    }
  
  return msg;
}


static gboolean 
wire_std_protocol_connect       (WireProtocol          *proto,
                                 Wire                  *wire)
{
  WireStdProtocol *wsp = (WireStdProtocol *) proto;
  gchar *magic = "WireStdProtocol";
  WireBuffer *in, *out;
  WireFixedBuffer *fixed_in, *fixed_out;
  guint32 in_ver, out_ver;
  gboolean success;

  g_return_val_if_fail (WIRE_IS_STD_PROTOCOL (proto), FALSE);

  out = wire_fixed_buffer_new_from_object ((guint8 *) magic, 16, NULL);
  fixed_out = (WireFixedBuffer *) out;

  if (! wire_protocol_send_buffer (proto->parent, proto, wire, out, NULL))
    return FALSE;
    
  if (! wire_protocol_flush (proto->parent, wire, NULL))
    return FALSE;

  in = wire_protocol_receive_buffer (proto->parent, proto, wire, NULL);

  if (!in)
    return FALSE;

  fixed_in = wire_buffer_read (in, NULL);

  if (!fixed_in)
    {
      g_object_unref (G_OBJECT (in));

      return FALSE;
    }
    
  if (fixed_in->size == fixed_out->size)
    success = memcmp (fixed_in->data, fixed_out->data, fixed_out->size) == 0;
  else
    success = FALSE;

  g_object_unref (G_OBJECT (in));
  g_object_unref (G_OBJECT (fixed_in));

  if (!success)
    return FALSE;
  
  out_ver = GUINT32_TO_LE (WIRE_STD_PROTOCOL_MAX_VERSION + 1);

  fixed_out->data = (guint8 *) &out_ver;

  fixed_out->size = 4;

  do
    {
      out_ver = GUINT32_TO_LE (GUINT32_FROM_LE (out_ver) - 1);
      
      if (!wire_protocol_send_buffer (proto->parent, proto, wire, out, NULL))
        out_ver = 0;

      if (!wire_protocol_flush (proto->parent, wire, NULL))
      	out_ver = 0;
      
      in = wire_protocol_receive_buffer (proto->parent, proto, wire, NULL); 

      if (!in)
        return FALSE;

      fixed_in = wire_buffer_read (in, NULL);

      if (!fixed_in)
        {
          g_object_unref (G_OBJECT (in));

          return FALSE;
        }
    
      if (fixed_in->size == 4)
        in_ver = * (guint32 *) fixed_in->data;
      else
        in_ver = 0;

      g_object_unref (G_OBJECT (in));
      g_object_unref (G_OBJECT (fixed_in));

    } while (out_ver != in_ver && in_ver != 0);
    
  return in_ver != 0;
}

static gboolean 
wire_std_protocol_disconnect    (WireProtocol          *proto,
                                 Wire                  *wire)
{
  return TRUE;
}
