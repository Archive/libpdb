/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_MSG_TYPE_H__
#define __WIRE_MSG_TYPE_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_MSG_TYPE            (wire_msg_type_get_type ())
#define WIRE_MSG_TYPE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_MSG_TYPE, WireMsgType))
#define WIRE_MSG_TYPE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_MSG_TYPE, WireMsgTypeClass))
#define WIRE_IS_MSG_TYPE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_MSG_TYPE))
#define WIRE_IS_MSG_TYPE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_MSG_TYPE))
#define WIRE_GET_MSG_TYPE_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_MSG_TYPE, WireMsgTypeClass))

struct _WireMsgType
{
  GObject  parent_instance;

  GType    type;
  
  gchar   *name;
};

struct _WireMsgTypeClass
{
  GObjectClass parent_class;
};

typedef struct _WireMsgTypeClass WireMsgTypeClass;

struct _WireMsgTypeTable
{
  GHashTable *type_table;
  GHashTable *id_table;

  guint next_id;
};

GType         wire_msg_type_get_type (void) G_GNUC_CONST;


WireMsgType * wire_msg_type_new      (GType          class_type,
                                      gchar         *msg_name);
WireMsg     * wire_msg_new_from_type (WireMsgType   *type);


WireMsgTypeTable * wire_msg_type_table_new (void);

void          wire_msg_type_table_destroy       (WireMsgTypeTable *msg_table);
void          wire_msg_type_table_insert        (WireMsgTypeTable *msg_table,
                                                 WireMsgType      *msg_type);
WireMsgType * wire_msg_type_table_lookup_type   (WireMsgTypeTable *msg_table,
                                                 guint32           msg_id);
guint32       wire_msg_type_table_lookup_id     (WireMsgTypeTable *msg_table,
                                                 GType             msg_type);

G_END_DECLS

#endif /* __WIRE_MSG_TYPE_H__ */

