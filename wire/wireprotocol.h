/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_PROTOCOL_H__
#define __WIRE_PROTOCOL_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_PROTOCOL            (wire_protocol_get_type ())
#define WIRE_PROTOCOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_PROTOCOL, WireProtocol))
#define WIRE_PROTOCOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_PROTOCOL, WireProtocolClass))
#define WIRE_IS_PROTOCOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_PROTOCOL))
#define WIRE_IS_PROTOCOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_PROTOCOL))
#define WIRE_GET_PROTOCOL_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_PROTOCOL, WireProtocolClass))

struct _WireProtocol {
  GObject       parent_instance;
  
  WireProtocol *parent;
  WireProtocol *sibling;
  WireProtocol *child;

  /* make a property? */
  gboolean      server;
};

struct _WireProtocolClass 
{
  GObjectClass  parent_class;

  /* virtual functions */
  gboolean     (* register_msg)        (WireProtocol *proto,
                                        WireMsgType  *msg);
                                    
  gboolean     (* add_subprotocol)     (WireProtocol *proto,
                                        WireProtocol *subproto);
  gboolean     (* query_superprotocol) (WireProtocol *proto,
                                        WireProtocol *superproto);
  gboolean     (* set_superprotocol)   (WireProtocol *proto,
                                        WireProtocol *superproto);
                                   
  gboolean     (* connect)             (WireProtocol *proto,
                                        Wire         *wire);
  gboolean     (* disconnect)          (WireProtocol *proto,
                                        Wire         *wire);
                                    
  gboolean     (* send)                (WireProtocol *protocol,
                                        Wire         *wire,
                                        WireMsg      *msg,
                                        GError      **error);
  WireMsg *    (* receive)             (WireProtocol *protocol,
                                        Wire         *wire,
                                        GError      **error);
  gboolean     (* flush)               (WireProtocol *proto,
                                        Wire         *wire,
                                        GError      **error);
  gboolean     (* send_buffer)         (WireProtocol *to,
                                        WireProtocol *from,
                                        Wire         *wire,
                                        WireBuffer   *buf,
                                        GError      **error);
  WireBuffer * (* receive_buffer)      (WireProtocol *to,
                                        WireProtocol *from,
                                        Wire         *wire,
                                        GError      **error);

};

typedef struct _WireProtocolClass WireProtocolClass;


GType        wire_protocol_get_type         (void) G_GNUC_CONST;

/* use GError? */
gboolean     wire_protocol_add_subprotocol  (WireProtocol *superproto,
                                             WireProtocol *subproto);

gboolean     wire_protocol_connect          (WireProtocol *proto,
                                             Wire         *wire);
gboolean     wire_protocol_disconnect       (WireProtocol *proto,
                                             Wire         *wire);
                                          
gboolean     wire_protocol_register_msg     (WireProtocol  *proto,
                                             WireMsg       *msg);
                                          
gboolean     wire_protocol_is_server        (WireProtocol *proto);

gboolean     wire_protocol_send             (WireProtocol *proto,
                                             Wire         *wire,
                                             WireMsg      *msg,
                                             GError      **error);
WireMsg *    wire_protocol_receive          (WireProtocol *proto,
                                             Wire         *wire,
                                             GError      **error);
gboolean     wire_protocol_flush            (WireProtocol *proto,
                                             Wire         *wire,
                                             GError      **error);
gboolean     wire_protocol_send_buffer      (WireProtocol *to,
                                             WireProtocol *from,
                                             Wire         *wire,
                                             WireBuffer   *buf,
                                             GError      **error);
WireBuffer * wire_protocol_receive_buffer   (WireProtocol *to,
                                             WireProtocol *from,
                                             Wire         *wire,
                                             GError      **error);


G_END_DECLS

#endif /* __WIRE_PROTOCOL_H__ */
