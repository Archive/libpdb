/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include "wiretypes.h"

#include "wirebuffer.h"

static void     wire_buffer_class_init                  (WireBufferClass   *klass); 
static void     wire_buffer_finalize                    (GObject           *object);

static GObjectClass  *parent_class;


GType
wire_buffer_get_type (void)
{
  static GType buffer_type = 0;

  if (! buffer_type)
    {
      static const GTypeInfo buffer_info =
      {
        sizeof (WireBufferClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) wire_buffer_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data     */
        sizeof (WireBuffer),
        0,              /* n_preallocs    */
        (GInstanceInitFunc) NULL,
      };

      buffer_type = g_type_register_static (G_TYPE_OBJECT,
                                            "WireBuffer", 
                                            &buffer_info, 
                                            G_TYPE_FLAG_ABSTRACT);
    }

  return buffer_type;
}

static void
wire_buffer_class_init (WireBufferClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = wire_buffer_finalize; 

  klass->read = NULL;
}

static void
wire_buffer_finalize (GObject *object)
{
  WireBuffer *buffer;

  g_return_if_fail (WIRE_IS_BUFFER (object));
  
  buffer = WIRE_BUFFER (object);
  
  if (buffer->sibling) 
    {
      g_object_unref (G_OBJECT (buffer->sibling));

      buffer->sibling = NULL;
    }

  if (buffer->child) 
    {
      g_object_unref (G_OBJECT (buffer->child));

      buffer->child = NULL;
    }




  G_OBJECT_CLASS (parent_class)->finalize (object);
}

WireFixedBuffer * 
wire_buffer_read     (WireBuffer  *buf,
                      GError     **error)
{
  WireBufferClass *buf_class = WIRE_GET_BUFFER_CLASS (buf);

  g_return_val_if_fail (buf != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_BUFFER (buf), NULL);

  return buf_class->read (buf, error);
}


gsize
wire_buffer_get_size (WireBuffer  *buf)
{
  WireBufferClass *buf_class = WIRE_GET_BUFFER_CLASS (buf);

  g_return_val_if_fail (buf != NULL, 0);
  g_return_val_if_fail (WIRE_IS_BUFFER (buf), 0);

  return buf_class->get_size (buf);
}

gsize
wire_buffer_get_total_size (WireBuffer *buf)
{
  gsize size;

  g_return_val_if_fail (buf != NULL, 0);
  g_return_val_if_fail (WIRE_IS_BUFFER (buf), 0);

  size = wire_buffer_get_size (buf);

  if (buf->child)
    size += wire_buffer_get_total_size (buf->child);

  if (buf->sibling)
    size += wire_buffer_get_total_size (buf->sibling);

  return size;
}

gboolean 
wire_buffer_foreach        (WireBuffer  *buf,
                            WBFunc       func,
                            gpointer     data,
                            GError     **error)
{
  WireFixedBuffer *fixedbuf;

  g_return_val_if_fail (buf != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_BUFFER (buf), FALSE);
  g_return_val_if_fail (func != NULL, FALSE);

  fixedbuf = wire_buffer_read (buf, error);
  
  if (! fixedbuf)
    return FALSE;

  if (! func (fixedbuf, data, error))
    {
      g_object_unref (G_OBJECT (fixedbuf));

      return FALSE;
    }

  g_object_unref (G_OBJECT (fixedbuf));

  if (buf->child)
    if (! wire_buffer_foreach (buf->child, func, data, error))
      return FALSE;

  if (buf->sibling)
    if (! wire_buffer_foreach (buf->sibling, func, data, error))
      return FALSE;

  return TRUE;
}
