/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <glib.h>

/* inherited from gimp code, probably no longer needed
#ifdef G_OS_WIN32
#include <process.h>
#include <io.h>
#endif
*/

#include "wiretypes.h"

#include "wirewire.h"
#include "wirebuffer.h"

#include "wireintl.h"

static void wire_init (Wire *wire);

GType
wire_get_type (void)
{
  static GType wire_type = 0;

  if (! wire_type)
    {
      static const GTypeInfo wire_info =
      {
        sizeof (WireClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) NULL,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (Wire),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) wire_init,
      };

      wire_type = g_type_register_static (G_TYPE_OBJECT,
	 				  "Wire", 
                                          &wire_info, G_TYPE_FLAG_ABSTRACT);
    }

  return wire_type;
}

/**
 * wire_send:
 * @wire: a #Wire.
 * @buf: the buffer to send.
 * @error: a location to return an error, or NULL.
 *
 * Sends @buf over @wire.
 * 
 * Returns: TRUE if the write was successful.
 */

gboolean
wire_send (Wire        *wire,
	   WireBuffer  *buf,
	   GError     **error)
{
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (buf != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_BUFFER (buf), FALSE);
  

  if (! WIRE_GET_CLASS (wire)->send (wire, buf, error))
    {
      wire->error_val = TRUE;
      return FALSE;
    }

  return TRUE;
}

/**
 * wire_receive:
 * @wire: a #Wire.
 * @error: a location to return an error, or NULL.
 *
 * Reads @count bytes from @wire.
 * 
 * Returns: a #WireBuffer, or NULL on error
 */
 
WireBuffer *
wire_receive (Wire       *wire,
	      GError    **error)
{
  WireBuffer *buf;

  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);

  buf = WIRE_GET_CLASS (wire)->receive (wire, error);

  if (!buf)
    {
      wire->error_val = TRUE;
      return NULL;
    }

  return buf;
}

/**
 * wire_flush:
 * @wire: a #Wire.
 * @error: a location to return an error, or NULL
 *
 * Flushes the #Wire.
 *
 * Returns: TRUE if the flush was successful.
 */
 
gboolean
wire_flush (Wire       *wire,
            GError    **error)
{
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  
  if (!(* WIRE_GET_CLASS(wire)->flush) (wire, error))
    {
      wire->error_val = TRUE;
      return FALSE;
    }

  return TRUE;
}

/**
 * wire_error:
 * @wire: a #Wire.
 * 
 * Checks the error status on the #Wire.
 *
 * Returns: TRUE if there is an error condition on @wire
 */
 
gboolean
wire_error (Wire *wire)
{
  g_return_val_if_fail (wire != NULL, TRUE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), TRUE);
  
  return wire->error_val;
}

/**
 * wire_clear_error:
 * @wire: a #Wire.
 * 
 * Clears the error status on the #Wire.
 */
 
void
wire_clear_error (Wire *wire)
{
  g_return_if_fail (wire != NULL);
  g_return_if_fail (WIRE_IS_WIRE (wire));

  wire->error_val = FALSE;
}

static void
wire_init (Wire *wire)
{
  g_return_if_fail (wire != NULL);
  g_return_if_fail (WIRE_IS_WIRE (wire));

  wire->error_val = FALSE;
  wire->protocol  = NULL;
}
