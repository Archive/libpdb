/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_WIRE_H__
#define __WIRE_WIRE_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_WIRE            (wire_get_type ())
#define WIRE(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_WIRE, Wire))
#define WIRE_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_WIRE, WireClass))
#define WIRE_IS_WIRE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_WIRE))
#define WIRE_IS_WIRE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_WIRE))
#define WIRE_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_WIRE, WireClass))


struct _Wire {
  GObject       parent_instance;
        
  /* FIXME: is error_val really the style of error handling we want? */
  gboolean       error_val;

  WireProtocol  *protocol;
};

struct _WireClass {
  GObjectClass  parent_class;

  gboolean     (* send)    (Wire        *wire,
                            WireBuffer  *buf,
                            GError     **error);
                     
  WireBuffer * (* receive) (Wire        *wire,
                            GError     **error);

  gboolean     (* flush)   (Wire        *wire,
                            GError     **error);

  /* FIXME: virtualize error clearing? */
};

typedef struct _WireClass WireClass;


GType         wire_get_type     (void) G_GNUC_CONST;

gboolean      wire_send         (Wire            *wire,
                                 WireBuffer      *buf,
                                 GError         **error);
WireBuffer *  wire_receive      (Wire            *wire,
                                 GError         **error);
gboolean      wire_flush        (Wire            *wire,
                                 GError         **error);

/* obsolete? */
gboolean      wire_error        (Wire            *wire);

void          wire_clear_error  (Wire            *wire);

G_END_DECLS

#endif /* __WIRE_WIRE_H__ */
