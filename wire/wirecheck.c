/* Libwire test cases */
/* Please note that some of these cases take advantage of 
   implementation-specific knowledge and are not examples of best programming
   practice */

#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include "wire.h"

int parent (int read_fd, int write_fd);
int child (int read_fd, int write_fd);

int 
main (int argc, char **argv) 
{
  int parent_to_child[2], child_to_parent[2];
  pid_t pid;


  if (pipe (parent_to_child) == -1 || pipe (child_to_parent) == -1)
    return 1;

  pid = fork ();

  if (pid == -1)
    return -1;

  if (pid) 
    {
      return parent (child_to_parent[0], parent_to_child[1]);
    }
  else
    {
      return child (parent_to_child[0], child_to_parent[1]);
    }
  
  return 1;
}

int
parent (int read_fd, int write_fd)
{
  GError *error = NULL;

  Wire *wire;
  WireProtocol *proto;
  WireStdProtocol *wsp;
  WireFixedBuffer *buf_in, *buf_out;
  
  GIOChannel *read_channel, *write_channel;
  
  gint32 int32_out = 0xFADEBEAD, int32_in = 0;

  g_type_init();

  read_channel  = g_io_channel_unix_new (read_fd);
  write_channel = g_io_channel_unix_new (write_fd);

  wire = wire_g_io_channel_new (read_channel, write_channel);
  
  proto = wire_std_protocol_new (FALSE);
  wsp   = WIRE_STD_PROTOCOL (proto);


  buf_out  = wire_fixed_buffer_new_from_object ((guint8 *) &int32_out,  4, NULL);

  g_print ("parent: testing raw reads/writes\n");
  
  if (! wire_send (wire, buf_out, &error))
    {
      g_error (error->message);
    
      return 1;
    }
    
  g_print ("parent: wrote 0x%8X\n", int32_out);

  if (!wire_flush (wire, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("parent: flushed wire\n");
  
  buf_in = (WireFixedBuffer *) wire_receive (wire, &error);
  
  if (! buf_in)
    {
      g_error (error->message);
    
      return 1;
    }

  int32_in = * (gint32 *) buf_in->data; 
  g_object_unref (G_OBJECT (buf_in));
    
  g_print ("parent: read 0x%8X\n", int32_in);

  if (int32_in != int32_out)
    return 1;

  g_print ("parent: testing std protocol\n");

  wire_protocol_connect (proto, wire);
#if 0
  
  if (!wire_msg_write_int32 (proto, wire, int32_out, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("parent: wrote 0x%8X\n", int32_out);

  if (! wire_protocol_flush (proto, wire, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("parent: flushed wire\n");

  int32_in = wire_msg_read_int32 (proto, wire, &error);

  if (error)
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("parent: read 0x%8X\n", int32_in);

  if (int32_in != int32_out)
    return 1;
#endif    
  return 0;
}

int
child (int read_fd, int write_fd)
{
  GError *error = NULL;

  Wire *wire;
  WireProtocol *proto;
  WireStdProtocol *wsp;
  
  GIOChannel *read_channel, *write_channel;
  
  WireFixedBuffer *buf;

  gint32 int32_buf;

  g_type_init ();

  read_channel  = g_io_channel_unix_new (read_fd);
  write_channel = g_io_channel_unix_new (write_fd);

  wire = wire_g_io_channel_new(read_channel, write_channel);

  proto = wire_std_protocol_new(FALSE);
  wsp   = WIRE_STD_PROTOCOL (proto);


  g_print ("child: testing raw reads/writes\n");

  buf = (WireFixedBuffer *) wire_receive (wire, &error);
  
  if (! buf)
    {
      g_error (error->message);
    
      return 1;
    }

  g_print ("child: read 0x%8X\n", * (gint32 *) buf->data);

  if (! wire_send (wire, (WireBuffer *) buf, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("child: wrote 0x%8X\n", * (gint32 *) buf->data);

  if (!wire_flush (wire, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("child: flushed wire\n");

  g_print ("child: testing protocol reads/writes\n");
  
  wire_protocol_connect (proto, wire);
#if 0  

  int32_buf = wire_msg_read_int32 (proto, wire, &error);

  if (error)
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("child: read 0x%8X\n", int32_buf);

  if (! wire_msg_write_int32 (proto, wire, int32_buf, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("child: wrote 0x%8X\n", int32_buf);

  if (! wire_protocol_flush (proto, wire, &error))
    {
      g_error (error->message);
    
      return 1;
    }
  g_print ("child: flushed wire\n");
#endif
  return 0;
}

