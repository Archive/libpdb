/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_TYPES_H__
#define __WIRE_TYPES_H__

typedef struct _WireBuffer       WireBuffer;
typedef struct _WireFixedBuffer  WireFixedBuffer;
typedef struct _WireStreamBuffer WireStreamBuffer;

typedef struct _Wire             Wire;
typedef struct _WireGIOChannel   WireGIOChannel;

typedef struct _WireProtocol     WireProtocol; 
typedef struct _WireStdProtocol  WireStdProtocol;

typedef struct _WireMsg          WireMsg;

typedef struct _WireMsgType      WireMsgType;
typedef struct _WireMsgTypeTable WireMsgTypeTable;

#endif /* __WIRE_TYPES_H__ */
