/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2002 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_G_IO_CHANNEL_H__
#define __WIRE_G_IO_CHANNEL_H__

#include "wirewire.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_G_IO_CHANNEL            (wire_g_io_channel_get_type ())
#define WIRE_G_IO_CHANNEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_G_IO_CHANNEL, WireGIOChannel))
#define WIRE_G_IO_CHANNEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_G_IO_CHANNEL, WireGIOChannelClass))
#define WIRE_IS_G_IO_CHANNEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_G_IO_CHANNEL))
#define WIRE_IS_G_IO_CHANNEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_G_IO_CHANNEL))
#define WIRE_GET_G_IO_CHANNEL_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_G_IO_CHANNEL, WireGIOChannelClass))

struct _WireGIOChannel {
	Wire         parent_instance;
	
	GIOChannel  *read;
	GIOChannel  *write;
};

struct _WireGIOChannelClass {
  WireClass  parent_class;
};

typedef struct _WireGIOChannelClass WireGIOChannelClass;


GType     wire_g_io_channel_get_type     (void) G_GNUC_CONST;

Wire    * wire_g_io_channel_new          (GIOChannel *read,
                                          GIOChannel *write);


G_END_DECLS

#endif /* __WIRE_G_IO_CHANNEL_H__ */
