/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_BUFFER_H__
#define __WIRE_BUFFER_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_BUFFER            (wire_buffer_get_type ())
#define WIRE_BUFFER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_BUFFER, WireBuffer))
#define WIRE_BUFFER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_BUFFER, WireBufferClass))
#define WIRE_IS_BUFFER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_BUFFER))
#define WIRE_IS_BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_BUFFER))
#define WIRE_GET_BUFFER_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_BUFFER, WireBufferClass))

struct _WireBuffer {
  GObject     parent_instance;

  WireBuffer *child;
  WireBuffer *sibling;

};

struct _WireBufferClass 
{
  GObjectClass  parent_class;

  /* virtual functions */
  WireFixedBuffer *    (* read)     (WireBuffer  *buf,
                                     GError     **error);
  gsize                (* get_size) (WireBuffer  *buf);
};

typedef struct _WireBufferClass WireBufferClass;


GType     wire_buffer_get_type  (void) G_GNUC_CONST;

/* FIXME: write some conveniece functions for adding children, siblings */


WireFixedBuffer * wire_buffer_read     (WireBuffer  *buf,
                                        GError     **error);
gsize             wire_buffer_get_size (WireBuffer  *buf);

/* convenience functions */

typedef gboolean (* WBFunc) (WireFixedBuffer  *buf,
                             gpointer          data,
                             GError          **error);

gsize    wire_buffer_get_total_size (WireBuffer  *buf);
gboolean wire_buffer_foreach        (WireBuffer  *buf,
                                     WBFunc       func,
                                     gpointer     data,
                                     GError     **error);

G_END_DECLS

#endif /* __WIRE_BUFFER_H__ */
