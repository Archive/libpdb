/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <glib.h>

#include "wiretypes.h"
#include "wiremsg.h"

#include "wirewire.h"
#include "wireprotocol.h"

#include "wiremarshal.h"

static void wire_msg_class_init (WireMsgClass *klass);

GType
wire_msg_get_type (void)
{
  static GType msg_type = 0;

  if (! msg_type)
    {
      static const GTypeInfo msg_info =
      {
        sizeof (WireMsgClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) wire_msg_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (WireMsg),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) NULL,
      };

      msg_type = g_type_register_static (G_TYPE_OBJECT,
	 			         "WireMsg", 
                                          &msg_info, 
                                          G_TYPE_FLAG_ABSTRACT);
    }

  return msg_type;
}

static void
wire_msg_class_init (WireMsgClass *klass)
{
/*  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass); */

/*  object_class->finalize = wire_protocol_finalize; */

  klass->read  = NULL;
  klass->write = NULL;
}


/**
 * wire_msg_read:
 * @msg: a #WireMsg. 
 * @proto: a #WireProtocol.
 * @wire: a #Wire.
 * @error: a location to store an error, or NULL.
 *
 * Reads a message from @wire using @proto, and sets
 * @msg accordingly. This function cannot be called before
 * wire_protocol_register_msg() is called with the message type used by
 * the incoming message.
 *
 * See Also: wire_protocol_register_msg(), wire_protocol_read_msg().
 *
 * Returns: TRUE on success.
 */
gboolean 
wire_msg_read (WireMsg        *msg,
               WireProtocol   *proto,
               Wire           *wire,
               GError        **error)
{
  WireMsgClass *msg_class = WIRE_GET_MSG_CLASS (msg);

  g_return_val_if_fail (msg != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_MSG (msg), FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (msg_class->read != NULL, FALSE);

  return msg_class->read (msg, proto, wire, error);
}

/**
 * wire_msg_write:
 * @msg: a #WireMsg.
 * @proto: a #WireProtocol.
 * @wire: a #Wire.
 * @msg: the #WireMsg to write.
 * @error: a location to store an error, or NULL.
 *
 * Writes @msg on @wire using @proto. This function cannot 
 * be called before wire_protocol_register_msg() is called with the message
 * type used by @msg.
 *
 * See Also: wire_protocol_register_msg(), wire_protocol_write_msg().
 *
 * Returns: TRUE on success.
 */
gboolean
wire_msg_write (WireMsg       *msg,
                WireProtocol  *proto,
                Wire          *wire,
                GError       **error)
{
  WireMsgClass *msg_class = WIRE_GET_MSG_CLASS (msg);

  g_return_val_if_fail (msg != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_MSG (msg), FALSE);
  g_return_val_if_fail (msg_class->write != NULL, FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  
  return msg_class->write (msg, proto, wire, error);
}
