/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_MSG_H__
#define __WIRE_MSG_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_MSG            (wire_msg_get_type ())
#define WIRE_MSG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_MSG, WireMsg))
#define WIRE_MSG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_MSG, WireMsgClass))
#define WIRE_IS_MSG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_MSG))
#define WIRE_IS_MSG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_MSG))
#define WIRE_GET_MSG_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_MSG, WireMsgClass))

struct _WireMsg
{
  GObject  parent_instance;
};

struct _WireMsgClass
{
  GObjectClass parent_class;

  gboolean (* read)    (WireMsg       *msg,
                        WireProtocol  *proto,
                        Wire          *wire,
                        GError       **error);
  gboolean (* write)   (WireMsg       *msg,
                        WireProtocol  *proto,
                        Wire          *wire,
                        GError       **error);
};

typedef struct _WireMsgClass WireMsgClass;

GType     wire_msg_get_type (void) G_GNUC_CONST;

gboolean  wire_msg_read          (WireMsg       *msg,
                                  WireProtocol  *proto,
                                  Wire          *wire,
                                  GError       **error);
gboolean  wire_msg_write         (WireMsg       *msg,
                                  WireProtocol  *proto,
                                  Wire          *wire,
                                  GError       **error);
G_END_DECLS

#endif /* __WIRE_MSG_H__ */

