/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <glib.h>

#ifdef G_OS_WIN32
#include <process.h>
#include <io.h>
#endif


#include "wiretypes.h"

#include "wireprotocol.h"
#include "wirewire.h"

#include "wiremarshal.h"

static void     wire_protocol_class_init                  (WireProtocolClass   *klass); 
static void     wire_protocol_finalize                    (GObject             *object);

static gboolean wire_protocol_default_add_subprotocol     (WireProtocol *proto,
                                                           WireProtocol *subproto);
static gboolean wire_protocol_default_query_superprotocol (WireProtocol *proto,
                                                           WireProtocol *superproto);
static gboolean wire_protocol_default_set_superprotocol   (WireProtocol *proto,
                                                           WireProtocol *superproto);
static gboolean wire_protocol_default_connect             (WireProtocol *proto,
                                                           Wire         *wire);
static gboolean wire_protocol_default_disconnect          (WireProtocol *proto,
                                                           Wire         *wire);

static gboolean  wire_protocol_default_send               (WireProtocol *proto,
                                                           Wire         *wire,
                                                           WireMsg      *msg,
                                                           GError      **error);
static WireMsg * wire_protocol_default_receive            (WireProtocol *proto,
                                                           Wire         *wire,
                                                           GError      **error);
static gboolean  wire_protocol_default_flush              (WireProtocol *proto,
                                                           Wire         *wire,
                                                           GError      **error);


static GObjectClass  *parent_class;


GType
wire_protocol_get_type (void)
{
  static GType proto_type = 0;

  if (! proto_type)
    {
      static const GTypeInfo proto_info =
      {
        sizeof (WireProtocolClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) wire_protocol_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (WireProtocol),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) NULL,
      };

      proto_type = g_type_register_static (G_TYPE_OBJECT,
	 				  "WireProtocol", 
                                          &proto_info, G_TYPE_FLAG_ABSTRACT);
    }

  return proto_type;
}

static void
wire_protocol_class_init (WireProtocolClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = wire_protocol_finalize; 


                  
  klass->add_subprotocol   = wire_protocol_default_add_subprotocol;
  klass->query_superprotocol = wire_protocol_default_query_superprotocol;
  klass->set_superprotocol   = wire_protocol_default_set_superprotocol;

  klass->connect    = wire_protocol_default_connect;
  klass->disconnect = wire_protocol_default_disconnect;

  klass->send    = wire_protocol_default_send;
  klass->receive = wire_protocol_default_receive;
  klass->flush   = wire_protocol_default_flush;
}

static void
wire_protocol_finalize (GObject *object)
{
  WireProtocol *proto;

  g_return_if_fail (WIRE_IS_PROTOCOL (object));
  
  proto = WIRE_PROTOCOL (object);
  
  if (proto->sibling) 
    {
      g_object_unref (G_OBJECT (proto->sibling));

      proto->sibling = NULL;
    }

  if (proto->child) 
    {
      g_object_unref (G_OBJECT (proto->child));

      proto->child = NULL;
    }




  G_OBJECT_CLASS (parent_class)->finalize (object);
}


/**
 * wire_protocol_add_subprotocol:
 * @superproto: a #WireProtocol.
 * @subproto: the #WireProtocol to add.
 *
 * Adds @subproto as a subprotocol of @superproto
 *
 * Returns: TRUE if the addition of @subproto was successful.
 */
 
gboolean
wire_protocol_add_subprotocol  (WireProtocol *superproto,
                                WireProtocol *subproto)
{
  WireProtocolClass *subclass, *superclass;

  g_return_val_if_fail (superproto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (superproto), FALSE);

  g_return_val_if_fail (subproto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (subproto), FALSE);

  subclass   = WIRE_GET_PROTOCOL_CLASS (subproto);
  superclass = WIRE_GET_PROTOCOL_CLASS (superproto);
  
  if (!subclass->query_superprotocol (subproto, superproto))
    return FALSE;

  if (!superclass->add_subprotocol (superproto, subproto))
    return FALSE;

  if (!subclass->set_superprotocol (subproto, superproto))
    return FALSE;

  g_object_ref (G_OBJECT (subproto));

  if (!superproto->child) 
    {
      superproto->child = subproto;
    }
  else
    {
      WireProtocol *p; 

      for (p = superproto->child; p->sibling != NULL; p = p->sibling)
        ;

      p->sibling = subproto;
    }

  return TRUE;
}

/**
 * wire_protocol_connect
 * @proto: a #WireProtocol. Must be the root of its tree.
 * @wire: a #Wire.
 *
 * Informs @proto that @wire will be used with it.  Any initialization
 * or handshaking necessary for @proto will be done at this time.
 *
 * There is no need to call wire_protocol_connect on the subprotocols
 * of @proto -- it will be done automagically.
 *
 * Returns: TRUE if the registration was successful.
 */
gboolean  
wire_protocol_connect (WireProtocol *proto,
                       Wire         *wire)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (proto->parent == NULL, FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (wire->protocol == NULL, FALSE);

  /* fixme do traversal here, don't set protocol on error */

  wire->protocol = proto;  

  return WIRE_GET_PROTOCOL_CLASS (proto)->connect (proto, wire);
}

/**
 * wire_protocol_disconnect
 * @proto: a #WireProtocol.
 * @wire: a #Wire.
 *
 * Informs @proto that @wire will be no longer used by it.  Any uninitialization
 * and connection closing necessary for @proto will be done at this time.
 *
 * There is no need to call wire_protocol_disconnect on the subprotocols
 * of @proto -- it will be done automagically.
 *
 * FIXME: use postorder traversal.
 *
 * Returns: TRUE if the unbinding was successful.
 */
gboolean  
wire_protocol_disconnect  (WireProtocol *proto,
                           Wire         *wire)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (proto->parent == NULL, FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (wire->protocol != NULL, FALSE);
  g_return_val_if_fail (wire->protocol == proto, FALSE);


  wire->protocol = NULL;
  
  return WIRE_GET_PROTOCOL_CLASS (proto)->disconnect (proto, wire);
}


/**
 * wire_protocol_is_server
 * @proto: a #WireProtocol.
 *
 * Returns whether @proto is a server or a client
 *
 * Returns: TRUE if @proto is a server.
 */
gboolean   
wire_protocol_is_server        (WireProtocol *proto)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  
  return proto->server;
}

/**
 * wire_protocol_send:
 * @proto: a #WireProtocol.
 * @wire: a #Wire.
 * @msg: the #WireMsg to write.
 * @error: a location to store an error, or NULL.
 *
 * Writes a message on @wire using @proto. This function cannot 
 * be called before wire_protocol_register_msg() is called with the message
 * type used by @msg.
 *
 * @msg->type must be set to the type of message prior to recieve prior to calling this function.
 *
 * See Also: wire_protocol_register_msg(), wire_protocol_receive().
 *
 * Returns: TRUE on success.
 */
gboolean
wire_protocol_send (WireProtocol  *proto,
                    Wire          *wire,
                    WireMsg       *msg,
                    GError       **error)
{
  WireProtocolClass *proto_class = WIRE_GET_PROTOCOL_CLASS (proto);

  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (msg != NULL, FALSE);
  g_return_val_if_fail (proto_class->send != NULL, FALSE);
  
  return proto_class->send (proto, wire, msg, error);
}

/**
 * wire_protocol_receive:
 * @proto: a #WireProtocol.
 * @wire: a #Wire.
 * @error: a location to store an error, or NULL.
 *
 * Reads a message from @wire using @proto. This function cannot 
 * be called before wire_protocol_register_msg() is called with the message
 * type used by the incomming message.
 *
 * See Also: wire_protocol_register_msg(), wire_protocol_send().
 *
 * Returns: a #WireMsg on success, or NULL on failure.
 */

WireMsg * 
wire_protocol_receive (WireProtocol   *proto,
                       Wire           *wire,
                       GError        **error)
{
  WireProtocolClass *proto_class = WIRE_GET_PROTOCOL_CLASS (proto);

  g_return_val_if_fail (wire != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), NULL);
  g_return_val_if_fail (proto != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), NULL);
  g_return_val_if_fail (proto_class->receive != NULL, NULL);

  return proto_class->receive (proto, wire, error);
}


/**
 * wire_protocol_flush
 * @proto: a #WireProtocol.
 * @wire: a #Wire to flush.
 * @error: a location to store an error, or NULL.
 *
 * Flushes the #Wire, doing whatever is needed on the protocol level to ensure 
 * all #Wiremsg s that have been sent are recieved.
 *
 * Returns: TRUE if the flush was successful.
 *
 * See Also: wire_flush()
 */
gboolean
wire_protocol_flush            (WireProtocol *proto,
                                Wire         *wire,
                                GError      **error)
{
  g_return_val_if_fail (proto == NULL || WIRE_IS_PROTOCOL (proto), FALSE);

  if (!proto)
    return wire_flush (wire, error);

  return WIRE_GET_PROTOCOL_CLASS (proto)->flush (proto, wire, error);
}

gboolean     
wire_protocol_send_buffer      (WireProtocol *to,
                                WireProtocol *from,
                                Wire         *wire,
                                WireBuffer   *buf,
                                GError      **error)
{
  /* FIXME g_return_if_fail */
  if (!to)
    return wire_send (wire, buf, error);

  return WIRE_GET_PROTOCOL_CLASS (to)->send_buffer (to, from, wire, buf, error);
}
                                
WireBuffer * 
wire_protocol_receive_buffer   (WireProtocol *from,
                                WireProtocol *to,
                                Wire         *wire,
                                GError      **error)
{
  if (!from)
    return wire_receive (wire, error);

  return WIRE_GET_PROTOCOL_CLASS (from)->receive_buffer (to, from, wire, error);
}


static gboolean 
wire_protocol_default_add_subprotocol     (WireProtocol *proto,
                                           WireProtocol *subproto)
{
 return TRUE;
}

static gboolean 
wire_protocol_default_query_superprotocol (WireProtocol *proto,
                                           WireProtocol *superproto)
{
  return TRUE;
}

static gboolean 
wire_protocol_default_set_superprotocol   (WireProtocol *proto,
                                           WireProtocol *superproto)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (superproto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (superproto), FALSE);
  g_return_val_if_fail (proto->parent == NULL, FALSE);

  proto->parent = superproto;

  return TRUE;
}


static gboolean 
wire_protocol_default_connect (WireProtocol *proto,
                               Wire         *wire)
{
  WireProtocol *p;
  
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);

  g_object_ref (G_OBJECT (wire));

  for (p = proto->child; p; p = p->sibling)  
    wire_protocol_connect (p, wire);

  return TRUE;
}

static gboolean 
wire_protocol_default_disconnect (WireProtocol *proto,
                                  Wire         *wire)
{
  WireProtocol *p;
  
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (wire != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);
  g_return_val_if_fail (WIRE_IS_WIRE (wire), FALSE);

  for (p = proto->child; p; p = p->sibling)  
    wire_protocol_disconnect (p, wire);
    
  g_object_unref (G_OBJECT (wire));

  return TRUE;
}

static gboolean  
wire_protocol_default_flush              (WireProtocol *proto,
                                          Wire         *wire,
                                          GError      **error)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);

  if (proto->parent)
    return WIRE_GET_PROTOCOL_CLASS (proto->parent)->flush (proto->parent, wire, error);

  return wire_flush (wire, error);
}


static gboolean
wire_protocol_default_send  (WireProtocol *proto,
                             Wire         *wire,
                             WireMsg      *msg,
                             GError      **error)
{
  g_return_val_if_fail (proto != NULL, FALSE);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), FALSE);

  if (proto->parent)
    return WIRE_GET_PROTOCOL_CLASS (proto->parent)->send (proto->parent, wire, 
                                                          msg, error);

  return wire_msg_write (msg, proto, wire, error);
}

static WireMsg *
wire_protocol_default_receive  (WireProtocol *proto,
                                Wire         *wire,
                                GError      **error)
{
  g_return_val_if_fail (proto != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_PROTOCOL (proto), NULL);

  if (proto->parent)
    return WIRE_GET_PROTOCOL_CLASS (proto->parent)->receive (proto->parent, wire, 
                                                          error);

  return wire_receive (wire, error);
}
