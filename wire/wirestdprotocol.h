/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_STD_PROTOCOL_H__
#define __WIRE_STD_PROTOCOL_H__

#include "wireprotocol.h"

G_BEGIN_DECLS

/* For information look into the C source or the html documentation */

#define WIRE_TYPE_STD_PROTOCOL            (wire_std_protocol_get_type ())
#define WIRE_STD_PROTOCOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WIRE_TYPE_STD_PROTOCOL, WireStdProtocol))
#define WIRE_STD_PROTOCOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WIRE_TYPE_STD_PROTOCOL, WireStdProtocolClass))
#define WIRE_IS_STD_PROTOCOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WIRE_TYPE_STD_PROTOCOL))
#define WIRE_IS_STD_PROTOCOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WIRE_TYPE_STD_PROTOCOL))
#define WIRE_GET_STD_PROTOCOL_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WIRE_TYPE_STD_PROTOCOL, WireStdProtocolClass))

#define WIRE_STD_PROTOCOL_MIN_VERSION 0x00000001
#define WIRE_STD_PROTOCOL_MAX_VERSION 0x00000001

struct _WireStdProtocol {
  WireProtocol  parent_instance;
  
  WireMsgTypeTable *msg_table;
};

struct _WireStdProtocolClass {
  WireProtocolClass  parent_class;
};

typedef struct _WireStdProtocolClass WireStdProtocolClass;

GType          wire_std_protocol_get_type (void) G_GNUC_CONST;
WireProtocol * wire_std_protocol_new      (gboolean server);


G_END_DECLS

#endif /* __WIRE_STD_PROTOCOL_H__ */
