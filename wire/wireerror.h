/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIRE_ERROR_H__
#define __WIRE_ERROR_H__

G_BEGIN_DECLS

typedef enum {
  WIRE_ERROR_UNEXPECTED_DISCONNECT /* remote process has disconnected unexpectedly */
} WireError;

typedef enum {
  WIRE_PROTOCOL_ERROR_WRONG_PROTOCOL,
  WIRE_PROTOCOL_ERROR_WRONG_VERSION
} WireProtocolError;

/**
 * WIRE_ERROR:
 *
 * Domain code for #Wire related errors.
 */
#define WIRE_ERROR wire_error_quark ()

GQuark    wire_error_quark  (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __WIRE_ERROR_H__*/
