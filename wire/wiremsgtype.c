/* LIBWIRE - The Wire RPC Library
 * Copyright (C) 1995-2003 Peter Mattis, Spencer Kimball, and Nathan Summers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h" 

#include "wiretypes.h"
#include "wiremsgtype.h"

static guint     wire_msg_type_table_hash_id      (guint32  *key);
static guint     wire_msg_type_table_hash_type    (GType    *key);
static gboolean  wire_msg_type_table_compare_id   (guint32  *a,
                                                   guint32  *b);
static gboolean  wire_msg_type_table_compare_type (GType    *a,
                                                   GType    *b);
                                                   
static void  wire_msg_type_table_free_id_entry    (gpointer  key,
                                                   gpointer  value,
                                                   gpointer  user_data);
static void  wire_msg_type_table_free_type_entry  (gpointer  key,
                                                   gpointer  value,
                                                   gpointer  user_data);

GType
wire_msg_type_get_type (void)
{
  static GType msg_type = 0;

  if (! msg_type)
    {
      static const GTypeInfo msg_info =
      {
        sizeof (WireMsgTypeClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) NULL,
	NULL,           /* class_finalize */
	NULL,           /* class_data     */
	sizeof (WireMsgType),
	0,              /* n_preallocs    */
	(GInstanceInitFunc) NULL,
      };

      msg_type = g_type_register_static (G_TYPE_OBJECT,
	 			         "WireMsgType", 
                                          &msg_info, 0);
    }

  return msg_type;
}

WireMsgType * 
wire_msg_type_new (GType  class_type,
                   gchar *msg_name)
{
  WireMsgType *msg_type;

  msg_type = g_object_new (WIRE_TYPE_MSG_TYPE, NULL);

  msg_type->type = class_type;
  msg_type->name = g_strdup (msg_name);

  return msg_type;
}

WireMsg * 
wire_msg_new_from_type (WireMsgType *type)
{
  g_return_val_if_fail (type != NULL, NULL);
  g_return_val_if_fail (WIRE_IS_MSG_TYPE (type), NULL);

  return g_object_new (type->type, NULL);
}

WireMsgTypeTable *
wire_msg_type_table_new (void)
{
  WireMsgTypeTable *msg_table = g_new (WireMsgTypeTable, 1);
  
  msg_table->type_table = 
    g_hash_table_new ((GHashFunc)    wire_msg_type_table_hash_id,
                      (GCompareFunc) wire_msg_type_table_compare_id);
                      
  msg_table->id_table = 
    g_hash_table_new ((GHashFunc)    wire_msg_type_table_hash_type,
                      (GCompareFunc) wire_msg_type_table_compare_type);

  msg_table->next_id = 1;

  return msg_table;
}

void
wire_msg_type_table_insert (WireMsgTypeTable *msg_table,
                            WireMsgType      *msg_type)
{
  guint32 *id;
  GType   *type;
  
  g_return_if_fail (msg_table != NULL);
  g_return_if_fail (msg_type != NULL);
  g_return_if_fail (WIRE_IS_MSG_TYPE (msg_type));

  id = g_hash_table_lookup (msg_table->id_table, &msg_type->type);
  
  if (!id)
  {
    id   = g_new (guint32, 1);
    type = g_new (GType, 1);

    *id   = msg_table->next_id++;
    *type = msg_type->type;

    g_object_ref (G_OBJECT (msg_type));

    g_hash_table_insert (msg_table->type_table, id, msg_type);
    g_hash_table_insert (msg_table->id_table, type, id);
  }
}


WireMsgType *
wire_msg_type_table_lookup_type (WireMsgTypeTable *msg_table,
                                 guint32           msg_id)
{
  WireMsgType *type;

  g_return_val_if_fail (msg_table != NULL, NULL);
  g_return_val_if_fail (msg_id != 0, NULL);

  return g_hash_table_lookup (msg_table->type_table, &msg_id);
  

}


guint32
wire_msg_type_table_lookup_id (WireMsgTypeTable *msg_table,
                               GType             msg_type)
{
  guint32 *id;
  
  g_return_val_if_fail (msg_table != NULL, 0);
  g_return_val_if_fail (WIRE_IS_MSG_TYPE (msg_type), 0);

  id = g_hash_table_lookup (msg_table->id_table, &msg_type);
  
  if (!id)
    return 0;

  return *id;
}


void
wire_msg_type_table_destroy (WireMsgTypeTable *msg_table)
{
  if (msg_table->type_table)
    {
      g_hash_table_foreach (msg_table->type_table, 
                            wire_msg_type_table_free_type_entry, NULL);
      g_hash_table_destroy (msg_table->type_table);

      msg_table->type_table = NULL;
    }
    
  if (msg_table->id_table)
    {
      g_hash_table_foreach (msg_table->id_table, 
                            wire_msg_type_table_free_id_entry, NULL);
      g_hash_table_destroy (msg_table->id_table);

      msg_table->id_table = NULL;
    }
}


static guint
wire_msg_type_table_hash_id (guint32 *key)
{
  return *key;
}

static guint
wire_msg_type_table_hash_type (GType *key)
{
  return *key;
}

static gboolean
wire_msg_type_table_compare_id (guint32 *a,
                                guint32 *b)
{
  return (*a == *b);
}

static gboolean
wire_msg_type_table_compare_type (GType *a,
                                  GType *b)
{
  return (*a == *b);
}

static void
wire_msg_type_table_free_id_entry (gpointer key,
                                   gpointer value,
                                   gpointer user_data)
{
  /* key not freed because it is the value for type_table */

  if (value)
    g_object_unref (G_OBJECT (value));
}

static void
wire_msg_type_table_free_type_entry (gpointer key,
                                     gpointer value,
                                     gpointer user_data)
{
  if (key)
    g_free (key);

  if (value)
    g_free (value);
}
